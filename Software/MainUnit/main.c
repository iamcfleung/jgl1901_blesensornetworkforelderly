/**
 * Copyright (c) 2016 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "nordic_common.h"
#include "nrf.h"
#include "nrf_drv_wdt.h"  
#include "nrf_fstorage.h"                                                       /**< Flash Storage */
#include "nrf_fstorage_sd.h" 
#include "app_error.h"
#include "app_uart.h"
#include "ble_db_discovery.h"
#include "app_timer.h"
#include "app_util.h"
#include "bsp_btn_ble.h"
#include "ble.h"
#include "ble_gap.h"
#include "ble_hci.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
//#include "ble_nus_c.h"
#include "nrf_ble_gatt.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_ble_scan.h"
#include "iot_timer.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define APP_DEBUG_MODE                  1
#define APP_BUTTON                      BSP_BOARD_BUTTON_0                      /**< Application Button. */
#define BUTTON_DEBOUNCE_INTERVAL        APP_TIMER_TICKS(250)                    /**< Single button debounce check period (250ms). */
#define WATCHDOG_FEED_INTERVAL          APP_TIMER_TICKS(5000)                   /**< Interval for feeding watchdog (5s). */
#define ASSOCIABLE_PERIOD_INTERVAL      APP_TIMER_TICKS(60000)                  /**< Interval for accepting association (60s). */
#define MODEM_COMM_INTERVAL             APP_TIMER_TICKS(1000)                    /**< Interval for accepting association (1000ms). */

#define FLASH_MAX_PERIPHERAL            128                                     /**< The maximumn number available peripheral address count in one single page memory. */
#define FLASH_PERIPHERAL_ADDR_START     0x80000                                 /**< The start address of flash memory for associate information (Central Address). */
#define FLASH_PERIPHERAL_ADDR_END       0x80fff                                 /**< The end address of flash memory for associate information (Central Address). */

#define APP_BLE_CONN_CFG_TAG            1                                       /**< Tag that refers to the BLE stack configuration set with @ref sd_ble_cfg_set. The default tag is @ref BLE_CONN_CFG_TAG_DEFAULT. */
#define APP_IPSP_TAG                    35                                      /**< Identifier for L2CAP configuration with the softdevice. */
#define APP_BLE_OBSERVER_PRIO           3                                       /**< BLE observer priority of the application. There is no need to modify this value. */

#define APP_CFG_ADV_DATA_LEN            23 
#define ADV_ENCODED_AD_TYPE_LEN         1                                       /**< Length of encoded ad type in advertisement data. */
#define ADV_ENCODED_AD_TYPE_LEN_LEN     1                                       /**< Length of the 'length field' of each ad type in advertisement data. */
#define ADV_FLAGS_LEN                   1                                       /**< Length of flags field that will be placed in advertisement data. */
#define ADV_ENCODED_FLAGS_LEN           (ADV_ENCODED_AD_TYPE_LEN +              \
                                        ADV_ENCODED_AD_TYPE_LEN_LEN +           \
                                        ADV_FLAGS_LEN)                          /**< Length of flags field in advertisement packet. (1 byte for encoded ad type plus 1 byte for length of flags plus the length of the flags itself). */
#define ADV_ENCODED_COMPANY_ID_LEN      2                                       /**< Length of the encoded Company Identifier in the Manufacturer Specific Data part of the advertisement data. */
#define ADV_ADDL_MANUF_DATA_LEN         (APP_CFG_ADV_DATA_LEN -                 \
                                        (                                       \
                                            ADV_ENCODED_FLAGS_LEN +             \
                                            (                                   \
                                                ADV_ENCODED_AD_TYPE_LEN +       \
                                                ADV_ENCODED_AD_TYPE_LEN_LEN +   \
                                                ADV_ENCODED_COMPANY_ID_LEN      \
                                            )                                   \
                                        )                                       \
                                        )                                       /**< Length of Manufacturer Specific Data field that will be placed on the air during advertisement. This is computed based on the value of APP_CFG_ADV_DATA_LEN (required advertisement data length). */

#define SCAN_INTERVAL                   0x000A                                  /**< Determines scan interval in units of 0.625 millisecond. */
#define SCAN_WINDOW                     0x000A                                  /**< Determines scan window in units of 0.625 millisecond. */
#define MIN_CONNECTION_INTERVAL         MSEC_TO_UNITS(30, UNIT_1_25_MS)         /**< Determines maximum connection interval in millisecond. */
#define MAX_CONNECTION_INTERVAL         MSEC_TO_UNITS(60, UNIT_1_25_MS)         /**< Determines maximum connection interval in millisecond. */
#define SLAVE_LATENCY                   0                                       /**< Determines slave latency in counts of connection events. */
#define SUPERVISION_TIMEOUT             MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Determines supervision time-out in units of 10 millisecond. */


#define APP_RX_PIN_NUMBER               27
#define APP_TX_PIN_NUMBER               26

#define UART_TX_BUF_SIZE                256                                     /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                     /**< UART RX buffer size. */

//#define NUS_SERVICE_UUID_TYPE   BLE_UUID_TYPE_VENDOR_BEGIN              /**< UUID type for the Nordic UART Service (vendor specific). */

//#define ECHOBACK_BLE_UART_DATA          1                                       /**< Echo the UART data that is received over the Nordic UART Service (NUS) back to the sender. */

APP_TIMER_DEF(m_button_debounce_id);                                            /**< Timer instance for detecting a button hold. */
APP_TIMER_DEF(m_wdt_feed_id);                                                   /**< Timer instance for watchdog feeding. */
APP_TIMER_DEF(m_associable_period_id);                                          /**< Timer instance for associable period. */
APP_TIMER_DEF(m_modem_comm_id);                                                  /**< Timer instance for talking to modem. */
//BLE_NUS_C_DEF(m_ble_nus_c);                                             /**< BLE Nordic UART Service (NUS) client instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                       /**< GATT module instance. */
BLE_DB_DISCOVERY_DEF(m_db_disc);                                                /**< Database discovery module instance. */
NRF_BLE_SCAN_DEF(m_scan);                                                       /**< Scanning Module instance. */


/* Static Global Variable Declaration
 *
 */

//static char const m_target_periph_name[] = "Nordic_Blinky";     /**< Name of the device we try to connect to. This name is searched in the scan report data*/
static uint8_t                      m_app_button_debounce_count;                /**< Counter for button debounce. */
static nrf_drv_wdt_channel_id       m_wdt_channel_id;                           /**< Watchdog channel. */
static const uint8_t                m_target_periph_addr[] = {0x84, 0xf7, 0xd4, 0xa4, 0x65, 0xcd};
static uint8_t                      m_associated_peripheral_count = 0;
static ble_gap_addr_t               m_associated_peripheral[FLASH_MAX_PERIPHERAL];
static uint8_t                      INVALID_ADDRESS[BLE_GAP_ADDR_LEN] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
static uint8_t                      ATOK[4] = {0x4f, 0x4b, 0x0d, 0x0a};
static const uint8_t                SSHSS_CONNECTABLE_ADV_MANUF_DATA[ADV_ADDL_MANUF_DATA_LEN] = {
                                        0x53, 0x53, 0x48, 0x53, 0x53, 0x43, 0x4f, 0x4e, 0x4e, 0x45, 0x43, 0x54, 0x41, 0x42, 0x4c, 0x45
                                    };
static bool                         m_clear_assocication = false;
static bool                         m_associable_mode = false;
static uint8_t                      m_modem_init = 0;
static bool                         m_modem_return_ok = false;
static char const                   m_modem_init_set_0[] = "ATE1\r\n";
static char const                   m_modem_init_set_1[] = "AT\r\n";
static char const                   m_modem_init_set_2[] = "AT+CGSOCKCONT=1,\"IP\",\"CMHK\"\r\n";
static char const                   m_modem_init_set_3[] = "AT+NETOPEN\r\n";
static char const                   m_modem_init_set_4[] = "AT+IPADDR\r\n";                                

                                    
                                    


/**
 * @brief Scan parameters requested for scanning and connection.
 */
static const ble_gap_scan_params_t m_scan_param =
{
     .active         = 0,                                                       // Passive scanning.
     .filter_policy  = BLE_GAP_SCAN_FP_ACCEPT_ALL,                              // Do not use whitelist.
     .interval       = (uint16_t)SCAN_INTERVAL,                                 // Scan interval.
     .window         = (uint16_t)SCAN_WINDOW,                                   // Scan window.
     .timeout        = 0,                                                      // Never stop scanning unless explicit asked to.
     .scan_phys      = BLE_GAP_PHY_AUTO                                         // Automatic PHY selection.
};


/* Custom Function Declaration. 
 * Declare functions that will be used across the whole application
 */
static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt);
static void flash_add_peripheral_address(uint8_t*);
static void flash_retrieve_all_peripheral_address(void);
static void flash_store_all_peripheral_address(void);
static void flash_delete_all_peripheral_address(void);
static uint32_t fstorage_len_modifier(uint32_t len);

NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
{
    /* Set a handler for fstorage events. */
    .evt_handler = fstorage_evt_handler,

    /* These below are the boundaries of the flash space assigned to this instance of fstorage.
     * You must set these manually, even at runtime, before nrf_fstorage_init() is called.
     * The function nrf5_flash_end_addr_get() can be used to retrieve the last address on the
     * last page of flash available to write data. */
    .start_addr = FLASH_PERIPHERAL_ADDR_START,
    .end_addr   = FLASH_PERIPHERAL_ADDR_END,
};

/**
 * @brief Connection parameters requested for connection.
 */
static const ble_gap_conn_params_t m_connection_param =
{
    .min_conn_interval = (uint16_t)MIN_CONNECTION_INTERVAL,                     // Minimum connection
    .max_conn_interval = (uint16_t)MAX_CONNECTION_INTERVAL,                     // Maximum connection
    .slave_latency     = SLAVE_LATENCY,                                         // Slave latency
    .conn_sup_timeout  = (uint16_t)SUPERVISION_TIMEOUT                          // Supervision time-out
};//static uint16_t m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - OPCODE_LENGTH - HANDLE_LENGTH; /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */



/**@brief NUS UUID. */
//static ble_uuid_t const m_nus_uuid =
//{
//    .uuid = BLE_UUID_NUS_SERVICE,
//    .type = NUS_SERVICE_UUID_TYPE
//};

//static ble_uuid_t const m_ble_uuid =
//{
//    .uuid = 0x180A,
//    .type = 0x01
//};


static bool peri_associated(ble_gap_addr_t const * peri_addr)
{
    for (uint32_t i = 0; i < m_associated_peripheral_count; i++)
    {
        if (memcmp(peri_addr->addr,
           m_associated_peripheral[i].addr,
            sizeof(m_associated_peripheral[i].addr)) == 0)
        {
            return true;
        }
    }
    return false;
}


/**
 * @brief WDT events handler.
 */
void wdt_event_handler(void)
{
    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("wdt_event_handler");
    #endif

    //NOTE: The max amount of time we can spend in WDT interrupt is two cycles of 32768[Hz] clock - after that, reset occurs
}


/**
 *  @brief Flash events handler.
 */
static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt)
{
    if (p_evt->result != NRF_SUCCESS)
    {
        #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("--> Event received: ERROR while executing an fstorage operation.");
        #endif
        return;
    }

    switch (p_evt->id)
    {
        case NRF_FSTORAGE_EVT_READ_RESULT:
        {
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("--> Event received: read %d bytes at address 0x%x.",
                         p_evt->len, p_evt->addr);
            #endif
            
            //NVIC_SystemReset();
        } break;
        
        case NRF_FSTORAGE_EVT_WRITE_RESULT:
        {
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("--> Event received: wrote %d bytes at address 0x%x.",
                         p_evt->len, p_evt->addr);
            #endif
            
            // reset when new pheriphal join
            NVIC_SystemReset();
        } break;

        case NRF_FSTORAGE_EVT_ERASE_RESULT:
        {
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("--> Event received: erased %d page from address 0x%x.",
                         p_evt->len, p_evt->addr);
            #endif
        } break;

        default:
            break;
    }
}


/**@brief Function for handling asserts in the SoftDevice.
 *
 * @details This function is called in case of an assert in the SoftDevice.
 *
 * @warning This handler is only an example and is not meant for the final product. You need to analyze
 *          how your product is supposed to react in case of assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num     Line number of the failing assert call.
 * @param[in] p_file_name  File name of the failing assert call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(0xDEADBEEF, line_num, p_file_name);
}


/**@brief Function for starting scanning. */
static void scan_start(void)
{
    ret_code_t ret;

    ret = nrf_ble_scan_start(&m_scan);
    APP_ERROR_CHECK(ret);

    ret = bsp_indication_set(BSP_INDICATE_SCANNING);
    APP_ERROR_CHECK(ret);

}


/**@brief Function for handling Scanning Module events.
 */
static void scan_evt_handler(scan_evt_t const * p_scan_evt)
{
    ret_code_t err_code;

    switch(p_scan_evt->scan_evt_id)
    {
        case NRF_BLE_SCAN_EVT_FILTER_MATCH:
        {
            //printf("NRF_BLE_SCAN_EVT_FILTER_MATCH");
            bsp_board_led_invert(BSP_BOARD_LED_2);
            //NRF_LOG_INFO("NRF_BLE_SCAN_EVT_FILTER_MATCH");
        } break;
            
        case NRF_BLE_SCAN_EVT_CONNECTING_ERROR:
        {
            //printf("NRF_BLE_SCAN_EVT_CONNECTING_ERROR");
            NRF_LOG_INFO("NRF_BLE_SCAN_EVT_CONNECTING_ERROR");
            err_code = p_scan_evt->params.connecting_err.err_code;
            APP_ERROR_CHECK(err_code);
        } break;

        case NRF_BLE_SCAN_EVT_CONNECTED:
        {
            //printf("NRF_BLE_SCAN_EVT_CONNECTED");
            ble_gap_evt_connected_t const * p_connected =
                                            p_scan_evt->params.connected.p_connected;
            // Scan is automatically stopped by the connection.
            NRF_LOG_INFO("Connected to target %02x%02x%02x%02x%02x%02x",
                p_connected->peer_addr.addr[0],
                p_connected->peer_addr.addr[1],
                p_connected->peer_addr.addr[2],
                p_connected->peer_addr.addr[3],
                p_connected->peer_addr.addr[4],
                p_connected->peer_addr.addr[5]
            );
            
            
        } break;

        case NRF_BLE_SCAN_EVT_SCAN_TIMEOUT:
        {
        NRF_LOG_INFO("Scan timed out.");
        } break;

        default:
            break;
    }
}


/**@brief Function for initializing the scanning and setting the filters.
 */
static void scan_init(void)
{
    ret_code_t          err_code;
    nrf_ble_scan_init_t init_scan;

    memset(&init_scan, 0, sizeof(init_scan));
    
    init_scan.connect_if_match          = false;
    init_scan.conn_cfg_tag              = APP_BLE_CONN_CFG_TAG;
    

    err_code = nrf_ble_scan_init(&m_scan, &init_scan, scan_evt_handler);
    APP_ERROR_CHECK(err_code);
    
    err_code = nrf_ble_scan_params_set(&m_scan, &m_scan_param);
    APP_ERROR_CHECK(err_code);

    // Setting filters for scanning.
    //err_code = nrf_ble_scan_filter_set(&m_scan, SCAN_UUID_FILTER, &m_ble_uuid);
    //APP_ERROR_CHECK(err_code);
    //
    //err_code = nrf_ble_scan_filter_set(&m_scan, SCAN_NAME_FILTER, &m_target_periph_name);
    //APP_ERROR_CHECK(err_code);
    
    err_code = nrf_ble_scan_filter_set(&m_scan, SCAN_ADDR_FILTER, &m_target_periph_addr);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_scan_filters_enable(&m_scan, 
                                            NRF_BLE_SCAN_ADDR_FILTER, 
                                            false);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling database discovery events.
 *
 * @details This function is a callback function to handle events from the database discovery module.
 *          Depending on the UUIDs that are discovered, this function forwards the events
 *          to their respective services.
 *
 * @param[in] p_event  Pointer to the database discovery event.
 */
static void db_disc_handler(ble_db_discovery_evt_t * p_evt)
{
    //ble_nus_c_on_db_disc_evt(&m_ble_nus_c, p_evt);
}


/**@brief Function for handling characters received by the Nordic UART Service (NUS).
 *
 * @details This function takes a list of characters of length data_len and prints the characters out on UART.
 *          If @ref ECHOBACK_BLE_UART_DATA is set, the data is sent back to sender.
 */
/*static void ble_nus_chars_received_uart_print(uint8_t * p_data, uint16_t data_len)
{
    ret_code_t ret_val;

    NRF_LOG_DEBUG("Receiving data.");
    NRF_LOG_HEXDUMP_DEBUG(p_data, data_len);

    for (uint32_t i = 0; i < data_len; i++)
    {
        do
        {
            ret_val = app_uart_put(p_data[i]);
            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
            {
                NRF_LOG_ERROR("app_uart_put failed for index 0x%04x.", i);
                APP_ERROR_CHECK(ret_val);
            }
        } while (ret_val == NRF_ERROR_BUSY);
    }
    if (p_data[data_len-1] == '\r')
    {
        while (app_uart_put('\n') == NRF_ERROR_BUSY);
    }
    if (ECHOBACK_BLE_UART_DATA)
    {
        // Send data back to the peripheral.
        do
        {
            ret_val = ble_nus_c_string_send(&m_ble_nus_c, p_data, data_len);
            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
            {
                NRF_LOG_ERROR("Failed sending NUS message. Error 0x%x. ", ret_val);
                APP_ERROR_CHECK(ret_val);
            }
        } while (ret_val == NRF_ERROR_BUSY);
    }
}*/


/**@brief   Function for handling app_uart events.
 *
 * @details This function receives a single character from the app_uart module and appends it to
 *          a string. The string is sent over BLE when the last character received is a
 *          'new line' '\n' (hex 0x0A) or if the string reaches the maximum data length.
 */
void uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t data_array[UART_RX_BUF_SIZE];
    static uint16_t index = 0;
    uint32_t ret_val;

    switch (p_event->evt_type)
    {
        /**@snippet [Handling data from UART] */
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&data_array[index]));
            index++;
        
            if ((data_array[index - 1] == '\n') || (index >= (UART_RX_BUF_SIZE)))
            {
                //NRF_LOG_DEBUG("Ready to send data over BLE NUS");
                //NRF_LOG_HEXDUMP_DEBUG(data_array, index);

                /*do
                {
                    ret_val = ble_nus_c_string_send(&m_ble_nus_c, data_array, index);
                    if ( (ret_val != NRF_ERROR_INVALID_STATE) && (ret_val != NRF_ERROR_RESOURCES) )
                    {
                        APP_ERROR_CHECK(ret_val);
                    }
                } while (ret_val == NRF_ERROR_RESOURCES);*/
                
                
                /*
                for (uint32_t i = 0; i < index; i++)
                {
                    do
                    {
                        ret_val = app_uart_put(data_array[i]);
                        if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
                        {
                            NRF_LOG_ERROR("app_uart_put failed for index 0x%04x.", i);
                            APP_ERROR_CHECK(ret_val);
                        }
                    } while (ret_val == NRF_ERROR_BUSY);
                }
                */
                if (memcmp(data_array, ATOK, 4) == 0)
                {
                    NRF_LOG_INFO("Yes UA");
                    m_modem_return_ok = true;
                }
                
                NRF_LOG_HEXDUMP_INFO(data_array, index);
                
                index = 0;
                //NRF_LOG_ERROR("", data_array);
            }
            break;

        /**@snippet [Handling data from UART] */
        case APP_UART_COMMUNICATION_ERROR:
            NRF_LOG_ERROR("Communication error occurred while handling UART.");
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            NRF_LOG_ERROR("Error occurred in FIFO module used by UART.");
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}


/**@brief Callback handling Nordic UART Service (NUS) client events.
 *
 * @details This function is called to notify the application of NUS client events.
 *
 * @param[in]   p_ble_nus_c   NUS client handle. This identifies the NUS client.
 * @param[in]   p_ble_nus_evt Pointer to the NUS client event.
 */

/**@snippet [Handling events from the ble_nus_c module] */
/*static void ble_nus_c_evt_handler(ble_nus_c_t * p_ble_nus_c, ble_nus_c_evt_t const * p_ble_nus_evt)
{
    ret_code_t err_code;

    switch (p_ble_nus_evt->evt_type)
    {
        case BLE_NUS_C_EVT_DISCOVERY_COMPLETE:
            NRF_LOG_INFO("Discovery complete.");
            err_code = ble_nus_c_handles_assign(p_ble_nus_c, p_ble_nus_evt->conn_handle, &p_ble_nus_evt->handles);
            APP_ERROR_CHECK(err_code);

            err_code = ble_nus_c_tx_notif_enable(p_ble_nus_c);
            APP_ERROR_CHECK(err_code);
            NRF_LOG_INFO("Connected to device with Nordic UART Service.");
            break;

        case BLE_NUS_C_EVT_NUS_TX_EVT:
            ble_nus_chars_received_uart_print(p_ble_nus_evt->p_data, p_ble_nus_evt->data_len);
            break;

        case BLE_NUS_C_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected.");
            scan_start();
            break;
    }
}*/
/**@snippet [Handling events from the ble_nus_c module] */


/**
 * @brief Function for handling shutdown events.
 *
 * @param[in]   event       Shutdown type.
 */
static bool shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
    ret_code_t err_code;

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    switch (event)
    {
        case NRF_PWR_MGMT_EVT_PREPARE_WAKEUP:
            // Prepare wakeup buttons.
            err_code = bsp_btn_ble_sleep_mode_prepare();
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(shutdown_handler, APP_SHUTDOWN_HANDLER_PRIORITY);


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t          err_code;
    ble_gap_evt_t const *p_gap_evt = &p_ble_evt->evt.gap_evt;
    //ble_gap_addr_t      peer_addr;
    
    uint8_t             manuf_data_len;
    uint8_t             *manuf_data;
    uint16_t            message_idx;
    uint16_t            battery_level;
    uint16_t            sensor1_data;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            //err_code = ble_nus_c_handles_assign(&m_ble_nus_c, p_ble_evt->evt.gap_evt.conn_handle, NULL);
            //APP_ERROR_CHECK(err_code);
        
            NRF_LOG_INFO("BLE_GAP_EVT_CONNECTED:");

            m_associable_mode = false;
        
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);

            // start discovery of services. The NUS Client waits for a discovery result
            //err_code = ble_db_discovery_start(&m_db_disc, p_ble_evt->evt.gap_evt.conn_handle);
            //APP_ERROR_CHECK(err_code);
        
            if (peri_associated(&p_gap_evt->params.connected.peer_addr) == false)
            {
                memcpy(&m_associated_peripheral[m_associated_peripheral_count].addr,
                        p_gap_evt->params.connected.peer_addr.addr,
                        sizeof(p_gap_evt->params.connected.peer_addr.addr));
                
                flash_add_peripheral_address(m_associated_peripheral[m_associated_peripheral_count].addr);
            }
            else
            {
                NVIC_SystemReset();
            }
            break;

        case BLE_GAP_EVT_DISCONNECTED:

            NRF_LOG_INFO("Disconnected. conn_handle: 0x%x, reason: 0x%x",
                         p_gap_evt->conn_handle,
                         p_gap_evt->params.disconnected.reason);
        
            scan_start();
            break;

        case BLE_GAP_EVT_TIMEOUT:
            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
            {
                NRF_LOG_INFO("Connection Request timed out.");
                
                scan_start();
            }
            break;
            
        case BLE_GAP_EVT_ADV_REPORT:
            manuf_data_len = p_gap_evt->params.adv_report.data.len;
            manuf_data = &p_gap_evt->params.adv_report.data.p_data[7];
        
            if (m_associable_mode == true) {
                if ((p_gap_evt->params.adv_report.type.connectable == 1) \
                    && memcmp(manuf_data, SSHSS_CONNECTABLE_ADV_MANUF_DATA, sizeof(SSHSS_CONNECTABLE_ADV_MANUF_DATA)) == 0)
                {
                    #ifdef APP_DEBUG_MODE
                        NRF_LOG_INFO("********** Connectable **********");
                    #endif
                    
                    err_code = sd_ble_gap_connect(&p_gap_evt->params.adv_report.peer_addr,
                                                  &m_scan_param,
                                                  &m_connection_param,
                                                  APP_BLE_CONN_CFG_TAG);
                    
                    if (err_code != NRF_SUCCESS)
                    {
                        NRF_LOG_INFO("Connection Request Failed, reason %d", err_code);
                    }
                }
            }

            if (peri_associated(&p_gap_evt->params.adv_report.peer_addr) == true \
                && p_gap_evt->params.adv_report.type.connectable == 0)
            {
                message_idx = (uint16_t)((manuf_data[0] << 8) + manuf_data[1]);
                battery_level = (uint16_t)((manuf_data[2] << 8) + manuf_data[3]);
                sensor1_data = (uint16_t)((manuf_data[4] << 8) + manuf_data[5]);
                
                NRF_LOG_INFO("Addr: %02x:%02x:%02x:%02x:%02x:%02x",
                    p_gap_evt->params.adv_report.peer_addr.addr[5],
                    p_gap_evt->params.adv_report.peer_addr.addr[4],
                    p_gap_evt->params.adv_report.peer_addr.addr[3],
                    p_gap_evt->params.adv_report.peer_addr.addr[2],
                    p_gap_evt->params.adv_report.peer_addr.addr[1],
                    p_gap_evt->params.adv_report.peer_addr.addr[0]
                );
                
                //NRF_LOG_INFO("(RSSI, message_idx, battery_level, sensor1_data)");
                //NRF_LOG_INFO("%d, %d, %d, %d", p_gap_evt->params.adv_report.rssi, message_idx, battery_level, sensor1_data);
                
                /*
                NRF_LOG_INFO("RSSI: %d", p_gap_evt->params.adv_report.rssi);
                NRF_LOG_INFO("message_idx : %d", message_idx);
                NRF_LOG_INFO("battery_level: %d", battery_level);
                NRF_LOG_INFO("sersor1_data: %d", sensor1_data);
                */
                /*
                NRF_LOG_INFO("Data: %02x %02x %02x %02x %02x %02x",
                    manuf_data[0],
                    manuf_data[1],
                    manuf_data[2],
                    manuf_data[3],
                    manuf_data[4],
                    manuf_data[5]
                );
                
                NRF_LOG_INFO("Data: %02x %02x %02x %02x %02x %02x",
                    manuf_data[6],
                    manuf_data[7],
                    manuf_data[8],
                    manuf_data[9],
                    manuf_data[10],
                    manuf_data[11]
                );
                
                NRF_LOG_INFO("Data: %02x %02x %02x %02x",
                    manuf_data[12],
                    manuf_data[13],
                    manuf_data[14],
                    manuf_data[15]
                );
                */
            }

            break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported.
            err_code = sd_ble_gap_sec_params_reply(p_ble_evt->evt.gap_evt.conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
            // Accepting parameters requested by peer.
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                                    &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED)
    {
        //NRF_LOG_INFO("ATT MTU exchange completed.");

        //m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        //NRF_LOG_INFO("Ble NUS max data length set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
    }
}


/**@brief Function for initializing the GATT library. */
void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_central_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in] event  Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
    //ret_code_t err_code;

    switch (event)
    {
        case BSP_EVENT_SLEEP:
            nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
            break;

        case BSP_EVENT_DISCONNECT:
            //err_code = sd_ble_gap_disconnect(m_ble_nus_c.conn_handle,
            //                                 BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            //if (err_code != NRF_ERROR_INVALID_STATE)
            //{
            //    APP_ERROR_CHECK(err_code);
            //}
            break;
        
        case BSP_EVENT_KEY_0:
            m_app_button_debounce_count = 0;
            app_timer_start(m_button_debounce_id, BUTTON_DEBOUNCE_INTERVAL, NULL);
            
            break; // BSP_EVENT_KEY_0

        default:
            break;
    }
}

/**@brief Function for initializing the UART. */
static void uart_init(void)
{
    ret_code_t err_code;

    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = APP_RX_PIN_NUMBER,
        .tx_pin_no    = APP_TX_PIN_NUMBER,
        .rts_pin_no   = RTS_PIN_NUMBER,
        .cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
        .use_parity   = false,
        .baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);
}


/**@brief Function for initializing the Nordic UART Service (NUS) client. */
/*static void nus_c_init(void)
{
    ret_code_t       err_code;
    ble_nus_c_init_t init;

    init.evt_handler = ble_nus_c_evt_handler;

    err_code = ble_nus_c_init(&m_ble_nus_c, &init);
    APP_ERROR_CHECK(err_code);
}*/


/**@brief Function for initializing buttons and leds. */
static void buttons_leds_init(void)
{
    ret_code_t err_code;
    //bsp_event_t startup_event;

    err_code = bsp_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    //err_code = bsp_btn_ble_init(NULL, &startup_event);
    //APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Button debounce timeout.
 *
 * @details This function will be called when first the button is pressed or during the debouncing
 *          period.
 *
 * @param[in]   p_context   Pointer used for passing some arbitrary information (context) from the
 *                          app_start_timer() call to the timeout handler.
 */
static void button_debounce_timeout_handler(void * p_context)
{
    UNUSED_PARAMETER(p_context);
    
    m_app_button_debounce_count++;

    if (bsp_button_is_pressed(APP_BUTTON))
    {
         
        if (m_app_button_debounce_count == 20)
        {
            // Subroutine for applicatino button long press (5s, even button is still holding). Long press means to erase assocication and re-associate again.
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("app_button_long_press_triggered.");
            #endif
            
            // erase assocation
            //fstorage_delete_associate_address();
            
            // accepte new peripheral (turn on associabltion mode)
            //m_associable_mode = true;
            //app_timer_start(m_associable_period_id, ASSOCIABLE_PERIOD_INTERVAL, NULL);
            //#ifdef APP_DEBUG_MODE
            //    NRF_LOG_INFO("m_associable_mode turn on.");
            //#endif
        }
        else if (m_app_button_debounce_count > 20)
        {
            m_app_button_debounce_count = 21;
        }
    }
    else
    {
        if (m_app_button_debounce_count >= 4 && m_app_button_debounce_count < 20)
        {
            // Subroutine for application button short press (between 1s and 5s, happen only when button is released)
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("app_button_short_press_triggered");
            #endif
            
            // accepte new peripheral (turn on associabltion mode)
            m_associable_mode = true;
            app_timer_start(m_associable_period_id, ASSOCIABLE_PERIOD_INTERVAL, NULL);
            #ifdef APP_DEBUG_MODE
                NRF_LOG_INFO("m_associable_mode turn on.");
            #endif
        }
        
        if (m_app_button_debounce_count >= 20)
        {
            // When Release from long press
            
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("app_button_long_press_triggered -> Release");
            #endif
            
            //NVIC_SystemReset();
        }
        
        app_timer_stop(m_button_debounce_id);
    }
}


/**@brief Function for handling the watchdog feed timeout.
 *
 * @details This function will be called every 5s.
 *
 * @param[in]   p_context   Pointer used for passing some arbitrary information (context) from the
 *                          app_start_timer() call to the timeout handler.
 */
static void watchdog_feed_timeout_handler(void * p_context)
{
    UNUSED_PARAMETER(p_context);
    //#ifdef APP_DEBUG_MODE
    //NRF_LOG_INFO("watchdog_feed_timeout_handler.");
    //#endif
    
    nrf_drv_wdt_channel_feed(m_wdt_channel_id);
}


/**@brief Function for handling the watchdog feed timeout.
 *
 * @details This function will be called every 5s.
 *
 * @param[in]   p_context   Pointer used for passing some arbitrary information (context) from the
 *                          app_start_timer() call to the timeout handler.
 */
static void associable_timeout_handler(void * p_context)
{
    UNUSED_PARAMETER(p_context);
    #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("associable_timeout_handler.");
    #endif
    
    m_associable_mode = false;
    app_timer_stop(m_associable_period_id);
    
    #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("m_associable_mode turn off.");
    #endif
}


/**@brief Function for handling the modem comm.
 *
 * @details This function will be called every 1000ms.
 *
 * @param[in]   p_context   Pointer used for passing some arbitrary information (context) from the
 *                          app_start_timer() call to the timeout handler.
 */
static void modem_comm_handler(void * p_context)
{
    ret_code_t ret_val;
    
    char tx_data[UART_TX_BUF_SIZE];
    uint8_t tx_data_len = 0;
    
    UNUSED_PARAMETER(p_context);
    #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("modem_comm_handler. %d %d", m_modem_init, m_modem_return_ok);
    #endif
    
    //m_associable_mode = false;
    //app_timer_stop(m_modem_comm_id);
    
    switch(m_modem_init)
    {
        case 0:
        {
            tx_data_len = sizeof(m_modem_init_set_0);
            memcpy(tx_data, m_modem_init_set_0, tx_data_len);
            
            for (uint16_t i = 0; i < tx_data_len; i++)
            {
                do
                {
                    ret_val = app_uart_put(tx_data[i]);
                    if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
                    {
                        NRF_LOG_ERROR("app_uart_put failed.");
                        APP_ERROR_CHECK(ret_val);
                    }
                } while (ret_val == NRF_ERROR_BUSY);
            }
            
            m_modem_init++;
        } break;
        
        case 1:
        {
            if (m_modem_return_ok == true)
            {
                m_modem_return_ok = false;
                
                tx_data_len = sizeof(m_modem_init_set_1);
                memcpy(tx_data, m_modem_init_set_1, tx_data_len);
                
                for (uint16_t i = 0; i < tx_data_len; i++)
                {
                    do
                    {
                        ret_val = app_uart_put(tx_data[i]);
                        if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
                        {
                            NRF_LOG_ERROR("app_uart_put failed.");
                            APP_ERROR_CHECK(ret_val);
                        }
                    } while (ret_val == NRF_ERROR_BUSY);
                }
                
                m_modem_init++;
            }
        } break;
        
        case 2:
        {
            if (m_modem_return_ok == true)
            {
                m_modem_return_ok = false;
                
                tx_data_len = sizeof(m_modem_init_set_2);
                memcpy(tx_data, m_modem_init_set_2, tx_data_len);
                
                for (uint16_t i = 0; i < tx_data_len; i++)
                {
                    do
                    {
                        ret_val = app_uart_put(tx_data[i]);
                        if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
                        {
                            NRF_LOG_ERROR("app_uart_put failed.");
                            APP_ERROR_CHECK(ret_val);
                        }
                    } while (ret_val == NRF_ERROR_BUSY);
                }
                
                m_modem_init++;
            }
        } break;
        
        case 3:
        {
            if (m_modem_return_ok == true)
            {
                m_modem_return_ok = false;
                
                tx_data_len = sizeof(m_modem_init_set_3);
                memcpy(tx_data, m_modem_init_set_3, tx_data_len);
                
                for (uint16_t i = 0; i < tx_data_len; i++)
                {
                    do
                    {
                        ret_val = app_uart_put(tx_data[i]);
                        if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
                        {
                            NRF_LOG_ERROR("app_uart_put failed.");
                            APP_ERROR_CHECK(ret_val);
                        }
                    } while (ret_val == NRF_ERROR_BUSY);
                }
                
                m_modem_init++;
            }
        } break;
        
        case 4:
        {
            if (m_modem_return_ok == true)
            {
                m_modem_return_ok = false;
                
                tx_data_len = sizeof(m_modem_init_set_4);
                memcpy(tx_data, m_modem_init_set_4, tx_data_len);
                
                for (uint16_t i = 0; i < tx_data_len; i++)
                {
                    do
                    {
                        ret_val = app_uart_put(tx_data[i]);
                        if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
                        {
                            NRF_LOG_ERROR("app_uart_put failed.");
                            APP_ERROR_CHECK(ret_val);
                        }
                    } while (ret_val == NRF_ERROR_BUSY);
                }
                
                m_modem_init++;
            }
        } break;
              
        default:
            break;
    }
    
    /*
    if (m_at_comm_empty != 0)
    {
        do
        {
            ret_val = app_uart_put(m_at_comm);
            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
            {
                NRF_LOG_ERROR("app_uart_put failed for index 0x%04x.");
                APP_ERROR_CHECK(ret_val);
            }
        } while (ret_val == NRF_ERROR_BUSY);
    }*/
    
    #ifdef APP_DEBUG_MODE
        //NRF_LOG_INFO("m_modem_comm turn off.");
    #endif
    
    ret_val = app_timer_start(m_modem_comm_id, MODEM_COMM_INTERVAL, NULL);
}


/**@brief Function for starting timers.
 */
static void application_timers_start(void)
{
    /* YOUR_JOB: Start your timers. below is an example of how to start a timer.
       ret_code_t err_code;
       err_code = app_timer_start(m_app_timer_id, TIMER_INTERVAL, NULL);
       APP_ERROR_CHECK(err_code); */
    
    ret_code_t err_code;

    err_code = app_timer_start(m_modem_comm_id, MODEM_COMM_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the timer. */
static void timers_init(void)
{
    // Initialize timer module.
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
    
    // Create timers.
    err_code = app_timer_create(&m_button_debounce_id,
                                APP_TIMER_MODE_REPEATED,
                                button_debounce_timeout_handler);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_wdt_feed_id,
                                APP_TIMER_MODE_REPEATED,
                                watchdog_feed_timeout_handler);
    APP_ERROR_CHECK(err_code);
    
    err_code = app_timer_create(&m_associable_period_id,
                                APP_TIMER_MODE_REPEATED,
                                associable_timeout_handler);
    APP_ERROR_CHECK(err_code);
    
    err_code = app_timer_create(&m_modem_comm_id,
                                APP_TIMER_MODE_REPEATED,
                                modem_comm_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the nrf log module. */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing watchdog.
 */
static void wdt_init(void)
{
    ret_code_t err_code;
    
    nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
    
    err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
    APP_ERROR_CHECK(err_code);
    
    err_code = nrf_drv_wdt_channel_alloc(&m_wdt_channel_id);
    APP_ERROR_CHECK(err_code);
}


/*
 *@brief Function for initializing fstorage.
 */
static void fstorage_init(void)
{
    ret_code_t err_code;
    
    nrf_fstorage_api_t* p_fs_api;
   
    p_fs_api = &nrf_fstorage_sd;
    
    err_code = nrf_fstorage_init(&fstorage, p_fs_api, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/** @brief Function for initializing the database discovery module. */
static void db_discovery_init(void)
{
    ret_code_t err_code = ble_db_discovery_init(db_disc_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details Handles any pending log operations, then sleeps until the next event occurs.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}


/**@brief Function for starting application timers.
 */
static void watchdog_start(void)
{
    // Start watchdog timers.
    nrf_drv_wdt_enable();
}


/**@brief Function to align data length to flash memory when writing in.
  */
static uint32_t fstorage_len_modifier(uint32_t len)
{
    if (len % sizeof(uint32_t))
    {
        return (len + sizeof(uint32_t) - (len % sizeof(uint32_t)));
    }

    return len;
}


/*
 *@brief Function to read association peripheral address.
 */
static void fstorage_read_peripheral_address(uint8_t* peri_addr, uint8_t peri_idx)
{
    ret_code_t err_code;
    
    uint32_t flash_peri_addr = FLASH_PERIPHERAL_ADDR_START + (peri_idx * fstorage_len_modifier(BLE_GAP_ADDR_LEN));;
    
    err_code = nrf_fstorage_read(&fstorage, flash_peri_addr, peri_addr, BLE_GAP_ADDR_LEN);

    if (err_code != NRF_SUCCESS)
    {
        #ifdef APP_DEBUG_MODE
            //NRF_LOG_INFO("fstorage_read_peripheral_address failed.");
        #endif
        
        return;
    }
    
    #ifdef APP_DEBUG_MODE
        //NRF_LOG_INFO("fstorage_read_peripheral_address.");
    #endif
}

static uint32_t peri_addr_u32[2];
/*
 *@brief Function to write association peripheral address.
 */
static void fstorage_write_peripheral_address(uint8_t* peri_addr, uint8_t peri_idx)
{
    ret_code_t err_code;

    uint32_t flash_peri_addr = FLASH_PERIPHERAL_ADDR_START + (peri_idx * fstorage_len_modifier(BLE_GAP_ADDR_LEN));

    peri_addr_u32[1] = (peri_addr[5] << 8) + peri_addr[4];
    peri_addr_u32[0] = (((((peri_addr[3] << 8) + peri_addr[2]) << 8) + peri_addr[1]) << 8) + peri_addr[0];

    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("fstorage_write_peripheral_address: %X:%X:%X:%X:%X:%X",
        peri_addr[5],
        peri_addr[4],
        peri_addr[3],
        peri_addr[2],
        peri_addr[1],
        peri_addr[0]);
    #endif

    err_code = nrf_fstorage_write(&fstorage, flash_peri_addr, &peri_addr_u32, sizeof(peri_addr_u32), NULL);
    
    if (err_code != NRF_SUCCESS)
    {
        #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("fstorage_write_associate_peripheral failed.");
        #endif
        
        return;
    }
    
    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("fstorage_write_peripheral_address.");
    #endif
}


/*
 *@brief Function to add association peripheral address.
 */
static void flash_add_peripheral_address(uint8_t* peri_addr)
{
    static uint8_t p_data[8];
    
    memset(p_data, 0x00, sizeof(p_data));
    memcpy(p_data, peri_addr, BLE_GAP_ADDR_LEN);
    
    fstorage_write_peripheral_address(p_data, m_associated_peripheral_count);
    
    m_associated_peripheral_count++;
}


/*
 *@brief Function to delete association peripheral address.
 */
//static void flash_delete_peripheral_address(uint8_t peri_idx)
//{
//    fstorage_write_peripheral_address(INVALID_ADDRESS, peri_idx);
    /*
    * To be Change
    *
    *
    *
    *
    */
//    m_associated_peripheral_count--;
//}


/*
 *@brief Function to retrieve all association peripheral address.
 */
static void flash_retrieve_all_peripheral_address(void)
{
    memset(m_associated_peripheral, 0xFF, sizeof(m_associated_peripheral));
    m_associated_peripheral_count = 0;
    
    for (uint8_t i = 0; i < FLASH_MAX_PERIPHERAL; i++)
    {
        fstorage_read_peripheral_address(m_associated_peripheral[i].addr, i);
        
        if (memcmp(m_associated_peripheral[i].addr, INVALID_ADDRESS, sizeof(BLE_GAP_ADDR_LEN)) != 0)
        {
            m_associated_peripheral_count++;
        }
        else
        {
            break;
        }
    }
}


/*
 *@brief Function to store all association peripheral address.
 */
static void flash_store_all_peripheral_address(void)
{
    flash_delete_all_peripheral_address();
    
    for (uint8_t i = 0; i < m_associated_peripheral_count; i++)
    {
        fstorage_write_peripheral_address(m_associated_peripheral[i].addr, i);
    }
}


/*
 *@brief Function to delete all association peripheral address.
 */
static void flash_delete_all_peripheral_address(void)
{
    ret_code_t err_code;
    
    err_code = nrf_fstorage_erase(&fstorage, FLASH_PERIPHERAL_ADDR_START, 1, NULL);
    if (err_code != NRF_SUCCESS)
    {
        #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("fstorage_delete_peripheral_address failed.");
        #endif
        
        memset(m_associated_peripheral, 0xFF, sizeof(m_associated_peripheral));
        m_associated_peripheral_count = 0;
        
        return;
    }
}


int main(void)
{
    ret_code_t ret_val;
    
    // Initialize.
    log_init();
    wdt_init();
    timers_init();
    
    ret_val = app_timer_start(m_wdt_feed_id, WATCHDOG_FEED_INTERVAL, NULL);
    APP_ERROR_CHECK(ret_val);
    
    uart_init();
    fstorage_init();

    buttons_leds_init();
    db_discovery_init();
    power_management_init();

    m_clear_assocication = bsp_board_button_state_get(APP_BUTTON);
    
    ble_stack_init();
    gatt_init();
    scan_init();

    // Start execution.
    #ifdef APP_DEBUG_MODE
        printf("SSHSS MU started.");
        NRF_LOG_INFO("SSHSS MU started.");
    #endif

    watchdog_start();

    if (m_clear_assocication)
    {
        flash_delete_all_peripheral_address();
        
        #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("flash_delete_all_peripheral_address");
        #endif
    }
    else
    {
        flash_retrieve_all_peripheral_address();
        
        #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("flash_retrieve_all_peripheral_address");
        #endif
    }
    
    if (m_associated_peripheral_count == 0)
    {
        m_associable_mode = true;
        app_timer_start(m_associable_period_id, ASSOCIABLE_PERIOD_INTERVAL, NULL);
        
        #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("m_associable_mode turn on.");
        #endif
    }else
    {
        
        
        for (uint8_t i = 0; i < m_associated_peripheral_count; i++)
        {
            #ifdef APP_DEBUG_MODE
                
                /*printf("Associated Peripheral Address: %X:%X:%X:%X:%X:%X",
                m_associated_peripheral[i].addr[5],
                m_associated_peripheral[i].addr[4],
                m_associated_peripheral[i].addr[3],
                m_associated_peripheral[i].addr[2],
                m_associated_peripheral[i].addr[1],
                m_associated_peripheral[i].addr[0]);*/
                for (uint8_t j = 0; j < 6; j++)
                {
                    do
                    {
                        ret_val = app_uart_put(m_associated_peripheral[i].addr[6-j]);
                        if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
                        {
                            NRF_LOG_ERROR("app_uart_put failed for index 0x%04x.", i);
                            APP_ERROR_CHECK(ret_val);
                        }
                    } while (ret_val == NRF_ERROR_BUSY);
                }
                app_uart_put('\n');

                NRF_LOG_INFO("Associated Peripheral Address: %X:%X:%X:%X:%X:%X",
                m_associated_peripheral[i].addr[5],
                m_associated_peripheral[i].addr[4],
                m_associated_peripheral[i].addr[3],
                m_associated_peripheral[i].addr[2],
                m_associated_peripheral[i].addr[1],
                m_associated_peripheral[i].addr[0]);
            #endif
        }
    }
    
    app_uart_put(m_associated_peripheral_count);
    
    scan_start();

    bsp_board_led_invert(BSP_BOARD_LED_1);
    application_timers_start();

    // Enter main loop.
    for (;;)
    {
        idle_state_handle();
    }
}
