/**
 * Copyright (c) 2014 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup ble_sdk_app_template_main main.c
 * @{
 * @ingroup ble_sdk_app_template
 * @brief Template project main file.
 *
 * This file contains a template for creating a new application. It has the code necessary to wakeup
 * from button, advertise, get a connection restart advertising on disconnect and if no new
 * connection created go back to system-off mode.
 * It can easily be used as a starting point for creating a new application, the comments identified
 * with 'YOUR_JOB' indicates where and how you can customize.
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nordic_common.h"
#include "nrf.h"
#include "nrf_drv_wdt.h"                                                        /**< Watchdog. */
#include "nrf_drv_saadc.h"                                                      /**< ADC. */
#include "nrf_fstorage.h"                                                       /**< Flash Storage */
#include "nrf_fstorage_sd.h"                                                    /**< Flash Storage */
#include "nrf_nvic.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
//#include "ble_bas.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "app_timer.h"
//#include "fds.h"
#include "peer_manager.h"
#include "peer_manager_handler.h"
#include "bsp_btn_ble.h"
#include "ble_conn_state.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


#define APP_DEBUG_MODE                  1
#define BUTTON_DEBOUNCE_INTERVAL        APP_TIMER_TICKS(250)                    /**< Single button debounce check period (250ms). */
#define SENSOR_DEBOUNCE_INTERVAL        APP_TIMER_TICKS(250)                    /**< Single sensor debounce check period (250ms). */
#define APP_STATUS_REPORT_INTERVAL      APP_TIMER_TICKS(10000)                  /**< Interval for advertising status report (5s). */
#define APP_STATUS_REPORT_SHORT_INTERVAL APP_TIMER_TICKS(1000)                  /**< Interval for advertising status report (2s). */
#define BATTERY_MEASURE_INTERVAL        APP_TIMER_TICKS(300000)                 /**< Battery measurement interval (300s). */
#define APP_BUTTON                      BSP_BOARD_BUTTON_0                      /**< Application Button. */
#define APP_SENSOR                      BSP_BOARD_BUTTON_1                      /**< Application Sensor1. */
#define APP_STATUS_LED                  BSP_BOARD_LED_0                         /**< Application LED. */

#define FLASH_ASSOCIATE_INFO_START      0x80000                                 /**< The start address of flash memory for associate information (Central Address). */
#define FLASH_ASSOCIATE_INFO_END        0x80fff                                 /**< The end address of flash memory for associate information (Central Address). */

#define ADC_REF_VOLTAGE_IN_MILLIVOLTS   600                                     /**< Reference voltage (in milli volts) used by ADC while doing conversion. */
#define ADC_PRE_SCALING_COMPENSATION    6                                       /**< The ADC is configured to use VDD with 1/3 prescaling as input. And hence the result of conversion is to be multiplied by 3 to get the actual value of the battery voltage.*/
#define DIODE_FWD_VOLT_DROP_MILLIVOLTS  270                                     /**< Typical forward voltage drop of the diode . */
#define ADC_RES_10BIT                   1024                                    /**< Maximum digital value for 10-bit ADC conversion. */

/**@brief Macro to convert the result of ADC conversion in millivolts.
 *
 * @param[in]  ADC_VALUE   ADC result.
 *
 * @retval     Result converted to millivolts.
 */
#define ADC_RESULT_IN_MILLI_VOLTS(ADC_VALUE)\
        ((((ADC_VALUE) * ADC_REF_VOLTAGE_IN_MILLIVOLTS) / ADC_RES_10BIT) * ADC_PRE_SCALING_COMPENSATION)


#define CON_DEVICE_NAME                 "PP_SSHSS"                              /**< Name of device when in connectable mode. Will be included in the advertising data. */
#define NCON_DEVICE_NAME                "PP_SSHSS"                              /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME               "Gleen Co Ltd"                          /**< Manufacturer. Will be passed to Device Information Service. */
#define COMPANY_IDENTIFIER              0xffff                                  /**< Company identifier for device under testing */

#define APP_CON_ADV_INTERVAL            MSEC_TO_UNITS(100, UNIT_0_625_MS)       /**< The advertising interval for connectable advertisement (100 ms). This value can vary between 100ms to 10.24s. */
#define APP_NCON_ADV_INTERVAL           80                                     /**< The advertising interval (in units of 0.625 ms. Max value = 10.24s ). */

#define APP_CON_ADV_DURATION            6000                                    /**< The advertising duration (60 seconds) in units of 10 milliseconds. */
#define APP_NCON_ADV_DURATION           8                                     /**< The advertising duration (1 second) in units of 10 milliseconds. */

#define APP_BLE_OBSERVER_PRIO           3                                       /**< Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_CONN_CFG_TAG            1                                       /**< A tag identifying the SoftDevice BLE configuration. */

#define APP_CFG_ADV_DATA_LEN            23                                      /**< Required length of the complete advertisement packet. This should be atleast 8 in order to accommodate flag field and other mandatory fields and one byte of manufacturer specific data. */
                                                                                /**< 23 is the number that allow 16 bytes manufactuer specific data */
#define ADV_ENCODED_AD_TYPE_LEN         1                                       /**< Length of encoded ad type in advertisement data. */
#define ADV_ENCODED_AD_TYPE_LEN_LEN     1                                       /**< Length of the 'length field' of each ad type in advertisement data. */
#define ADV_FLAGS_LEN                   1                                       /**< Length of flags field that will be placed in advertisement data. */
#define ADV_ENCODED_FLAGS_LEN           (ADV_ENCODED_AD_TYPE_LEN +              \
                                        ADV_ENCODED_AD_TYPE_LEN_LEN +           \
                                        ADV_FLAGS_LEN)                          /**< Length of flags field in advertisement packet. (1 byte for encoded ad type plus 1 byte for length of flags plus the length of the flags itself). */
#define ADV_ENCODED_COMPANY_ID_LEN      2                                       /**< Length of the encoded Company Identifier in the Manufacturer Specific Data part of the advertisement data. */
#define ADV_ADDL_MANUF_DATA_LEN         (APP_CFG_ADV_DATA_LEN -                 \
                                        (                                       \
                                            ADV_ENCODED_FLAGS_LEN +             \
                                            (                                   \
                                                ADV_ENCODED_AD_TYPE_LEN +       \
                                                ADV_ENCODED_AD_TYPE_LEN_LEN +   \
                                                ADV_ENCODED_COMPANY_ID_LEN      \
                                            )                                   \
                                        )                                       \
                                        )                                       /**< Length of Manufacturer Specific Data field that will be placed on the air during advertisement. This is computed based on the value of APP_CFG_ADV_DATA_LEN (required advertisement data length). */


#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(100, UNIT_1_25_MS)        /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(200, UNIT_1_25_MS)        /**< Maximum acceptable connection interval (0.2 second). */
#define SLAVE_LATENCY                   0                                       /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                   /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                  /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                       /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                  1                                       /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                       /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                  0                                       /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS              0                                       /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                    /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                       /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                       /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                      /**< Maximum encryption key size. */

#define DEAD_BEEF                       0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */


typedef struct
{
    bool            is_associated;
    ble_gap_addr_t  ble_gap_addr;
} sshss_association_t;


/* Services Declaration. For example:
 *  BLE_XYZ_DEF(m_xyz);
 */
APP_TIMER_DEF(m_button_debounce_id);                                            /**< Timer instance for detecting a button hold. */
APP_TIMER_DEF(m_sensor_debounce_id);                                            /**< Timer instance for detecting a sensor being activated. */
APP_TIMER_DEF(m_status_report_id);                                              /**< Timer instance for status report. */
APP_TIMER_DEF(m_battery_measure_id);                                            /**< Timer instance for advertising module. */

//BLE_BAS_DEF(m_bas);                                                             /**< Structure used to identify the battery service. */
NRF_BLE_GATT_DEF(m_gatt);                                                       /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                                         /**< Context for the Queued Write module.*/
BLE_ADVERTISING_DEF(m_advertising);                                             /**< Advertising module instance. */


// Services UUIDs used in application.
//static ble_uuid_t m_adv_uuids[] =                                               /**< Universally unique service identifiers. */
//{
//    {0x1234, BLE_UUID_TYPE_VENDOR_BEGIN}
//};


/* Static Global Variable Declaration
 *
 */
static uint8_t                      m_app_button_debounce_count;                        /**< Counter for button debounce. */
static bool                         m_app_sensor_is_debouncing = false;                 /**< Check sensor if debouncing. */
static uint8_t                      m_app_sensor_debounce_count;                        /**< Counter for sensor debounce. */
static nrf_drv_wdt_channel_id       m_wdt_channel_id;                                   /**< Watchdog channel. */
static nrf_saadc_value_t            adc_buf[2];                                         /**< Battery level ADC buffer. */
static uint16_t                     m_battery_level;                                    /**< Battery level. */
static uint16_t                     m_sensor1_data = 0;                                 /**< Sensor 1 Data */

static ble_gap_adv_params_t         m_adv_params;                                       /**< Parameters to be passed to the stack when starting advertising. */
static uint8_t                      m_addl_adv_manuf_data[ADV_ADDL_MANUF_DATA_LEN];     /**< Value of the additional manufacturer specific data that will be placed in air (initialized to all zeros). */
static uint16_t                     m_conn_handle = BLE_CONN_HANDLE_INVALID;            /**< Handle of the current connection. */
static uint8_t                      m_adv_handle = BLE_GAP_ADV_SET_HANDLE_NOT_SET;      /**< Advertising handle used to identify an advertising set. */
static uint8_t                      m_enc_advdata[APP_CFG_ADV_DATA_LEN];                /**< Buffer for storing an encoded advertising set. */
static bool                         m_isAdvertising = false;

static uint8_t                      m_plain_text[APP_CFG_ADV_DATA_LEN];
static uint8_t                      m_key[APP_CFG_ADV_DATA_LEN];
static uint8_t                      m_chiptext[APP_CFG_ADV_DATA_LEN];

/**@brief Struct that contains pointers to the encoded advertising data. */
static ble_gap_adv_data_t           m_adv_data = {
                                        .adv_data =
                                        {
                                            .p_data = m_enc_advdata,
                                            .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX
                                        },
                                        .scan_rsp_data =
                                        {
                                            .p_data = NULL,
                                            .len    = 0

                                        }
                                    };

static sshss_association_t          m_association;
static bool                         m_association_mode = false;
static const uint8_t                EMPTY_ASSOCIATE_ADDRESS[BLE_GAP_ADDR_LEN] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};     
static uint16_t                     m_message_idx = 0;
static const uint8_t                SSHSS_CONNECTABLE_ADV_MANUF_DATA[ADV_ADDL_MANUF_DATA_LEN] = {   0x53, 0x53, 0x48, 0x53,
                                                                                                    0x53, 0x43, 0x4f, 0x4e,
                                                                                                    0x4e, 0x45, 0x43, 0x54,
                                                                                                    0x41, 0x42, 0x4c, 0x45};

/* Custom Function Declaration. 
 * Declare functions that will be used across the whole application
 */
//static void advertising_start(bool erase_bonds);
static void gap_params_init(void);
static void connectable_adv_init(void);
static void non_connectable_adv_init(void);
static void advertising_init(void);
static void advdata_update(void);
static void advertising_start(void);
static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt);
static void fstorage_read_associate_address(uint8_t* asso_addr);
static void fstorage_write_associate_address(uint8_t* asso_addr);
static void fstorage_delete_associate_address(void);
//static void fstorage_add_peripheral(uint8_t* peri_count, uint8_t* peri_addr);
static uint32_t fstorage_len_modifier(uint32_t len);

NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
{
    /* Set a handler for fstorage events. */
    .evt_handler = fstorage_evt_handler,

    /* These below are the boundaries of the flash space assigned to this instance of fstorage.
     * You must set these manually, even at runtime, before nrf_fstorage_init() is called.
     * The function nrf5_flash_end_addr_get() can be used to retrieve the last address on the
     * last page of flash available to write data. */
    .start_addr = FLASH_ASSOCIATE_INFO_START,
    .end_addr   = FLASH_ASSOCIATE_INFO_END,
};


/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    pm_handler_on_pm_evt(p_evt);
    pm_handler_flash_clean(p_evt);

    switch (p_evt->evt_id)
    {
        case PM_EVT_PEERS_DELETE_SUCCEEDED:
            //advertising_start(false);
            advertising_start();
            break;

        default:
            break;
    }
}


/**
 * @brief WDT events handler.
 */
void wdt_event_handler(void)
{
    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("wdt_event_handler");
    #endif

    //NOTE: The max amount of time we can spend in WDT interrupt is two cycles of 32768[Hz] clock - after that, reset occurs
}


/**
 *  @brief Flash events handler.
 */
static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt)
{
    if (p_evt->result != NRF_SUCCESS)
    {
        #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("--> Event received: ERROR while executing an fstorage operation.");
        #endif
        return;
    }

    switch (p_evt->id)
    {
        case NRF_FSTORAGE_EVT_WRITE_RESULT:
        {
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("--> Event received: wrote %d bytes at address 0x%x.",
                         p_evt->len, p_evt->addr);
            #endif
            
            NVIC_SystemReset();
        } break;

        case NRF_FSTORAGE_EVT_ERASE_RESULT:
        {
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("--> Event received: erased %d page from address 0x%x.",
                         p_evt->len, p_evt->addr);
            #endif
        } break;

        default:
            break;
    }
}


/**@brief Function for handling the ADC interrupt.
 *
 * @details  This function will fetch the conversion result from the ADC, convert the value into
 *           percentage and send it to peer.
 */
void saadc_event_handler(nrf_drv_saadc_evt_t const * p_event)
{
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        nrf_saadc_value_t adc_result;
        //uint16_t          batt_lvl_in_milli_volts;
        //uint8_t           percentage_batt_lvl;
        uint32_t          err_code;

        adc_result = p_event->data.done.p_buffer[0];

        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, 1);
        APP_ERROR_CHECK(err_code);
        
        m_battery_level = ADC_RESULT_IN_MILLI_VOLTS(adc_result) + DIODE_FWD_VOLT_DROP_MILLIVOLTS;

        //batt_lvl_in_milli_volts = ADC_RESULT_IN_MILLI_VOLTS(adc_result) +
        //                          DIODE_FWD_VOLT_DROP_MILLIVOLTS;
        //percentage_batt_lvl = battery_level_in_percent(batt_lvl_in_milli_volts);
        
        #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("m_battery_level: %d", m_battery_level);
        //NRF_LOG_INFO("percentage_batt_lvl: %d", percentage_batt_lvl);
        //NRF_LOG_INFO("batt_lvl_in_milli_volts: %d", batt_lvl_in_milli_volts);
        #endif
        
        /*err_code = ble_bas_battery_level_update(&m_bas, percentage_batt_lvl, BLE_CONN_HANDLE_ALL);
        if ((err_code != NRF_SUCCESS) &&
            (err_code != NRF_ERROR_INVALID_STATE) &&
            (err_code != NRF_ERROR_RESOURCES) &&
            (err_code != NRF_ERROR_BUSY) &&
            (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
           )
        {
            APP_ERROR_HANDLER(err_code);
        }*/
    }
}


/**@brief Function for configuring ADC to do battery level conversion.
 */
static void adc_configure(void)
{
    ret_code_t err_code = nrf_drv_saadc_init(NULL, saadc_event_handler);
    APP_ERROR_CHECK(err_code);

    nrf_saadc_channel_config_t config =
        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_VDD);
    err_code = nrf_drv_saadc_channel_init(0, &config);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(&adc_buf[0], 1);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(&adc_buf[1], 1);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Button debounce timeout.
 *
 * @details This function will be called when first the button is pressed or during the debouncing
 *          period.
 *
 * @param[in]   p_context   Pointer used for passing some arbitrary information (context) from the
 *                          app_start_timer() call to the timeout handler.
 */
static void button_debounce_timeout_handler(void * p_context)
{
    ret_code_t err_code;
    
    UNUSED_PARAMETER(p_context);
    
    m_app_button_debounce_count++;

    if (bsp_button_is_pressed(APP_BUTTON))
    {
        if (m_app_button_debounce_count == 20)
        {
            // Subroutine for applicatino button long press (5s, even button is still holding). Long press means to erase assocication and re-associate again.
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("app_button_long_press_triggered. Reset when button relase");
            #endif
            
            // erase assocation
            fstorage_delete_associate_address();
            
            // clear is_assocaited status
            /*m_association.is_associated = false;
            
            connectable_adv_init();
            
            advertising_init();
            
            advertising_start();
            
            app_timer_stop(m_button_debounce_id);
            */
        }
        else if (m_app_button_debounce_count > 20)
        {
            m_app_button_debounce_count = 20;
        }
    }
    else
    {
        if (m_app_button_debounce_count >= 4 && m_app_button_debounce_count < 20)
        {
            if (m_association.is_associated == true)
            {
                app_timer_stop(m_status_report_id);
                
                // Subroutine for application button short press (between 1s and 5s, happen only when button is released)
                #ifdef APP_DEBUG_MODE
                NRF_LOG_INFO("app_button_short_press_triggered");
                #endif
                
                /*if (m_sensor1_data == 0)
                {
                    m_sensor1_data = 1;
                    bsp_board_led_on(BSP_BOARD_LED_3);
                }
                else
                {
                    m_sensor1_data = 0;
                    bsp_board_led_off(BSP_BOARD_LED_3);
                }*/
                /*
                advdata_update();
                
                while (m_isAdvertising == true);
                #ifdef APP_DEBUG_MODE
                NRF_LOG_INFO("m_isAdvertising: %d", m_isAdvertising);
                #endif
                advertising_start();
                #ifdef APP_DEBUG_MODE
                NRF_LOG_INFO("m_isAdvertising: %d", m_isAdvertising);
                #endif
                */
                err_code = app_timer_start(m_status_report_id, APP_STATUS_REPORT_SHORT_INTERVAL, NULL);
                APP_ERROR_CHECK(err_code);
            }
        }
        
        if (m_app_button_debounce_count >= 20)
        {
            // When Release from long press
            
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("app_button_long_press_triggered -> Reset");
            #endif
            
            NVIC_SystemReset();
        }
        
        app_timer_stop(m_button_debounce_id);
    }
}


/**@brief Function for handling the Sensor debounce timeout.
 *
 * @details This function will be called when first the sensor is being activated or during the debouncing
 *          period.
 *
 * @param[in]   p_context   Pointer used for passing some arbitrary information (context) from the
 *                          app_start_timer() call to the timeout handler.
 */
static void sensor_debounce_timeout_handler(void * p_context)
{
    //ret_code_t err_code;
    
    UNUSED_PARAMETER(p_context);
    
    //m_app_sensor_debounce_count++;

    if (bsp_button_is_pressed(APP_SENSOR) && m_sensor1_data == 0)
    {
        bsp_board_led_on(BSP_BOARD_LED_0);
        
        m_app_sensor_debounce_count++;
         
        if (m_app_sensor_debounce_count == 40)
        {
            // Subroutine for application sensor long activated (10s, even sensor is still being activated).
            //app_timer_stop(m_status_report_id);
            
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("app_sensor_long_activated_triggered.");
            #endif
            
            m_sensor1_data = 1;
            m_app_sensor_is_debouncing = false;
            bsp_board_led_on(BSP_BOARD_LED_3);
            
            //err_code = app_timer_start(m_status_report_id, APP_STATUS_REPORT_SHORT_INTERVAL, NULL);
            //APP_ERROR_CHECK(err_code);
            
            app_timer_stop(m_sensor_debounce_id);
        }
    }
    else if ((!bsp_button_is_pressed(APP_SENSOR)) && m_sensor1_data == 1)
    {
        bsp_board_led_off(BSP_BOARD_LED_0);
        
        m_app_sensor_debounce_count--;
        
        if (m_app_sensor_debounce_count <= 0)
        {
            // Subroutine for application sensor long released (10s, even sensor is still being released).
            //app_timer_stop(m_status_report_id);
            
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("app_sensor_long_released_triggered.");
            #endif
            
            m_sensor1_data = 0;
            m_app_sensor_is_debouncing = false;
            bsp_board_led_off(BSP_BOARD_LED_3);
            
            //err_code = app_timer_start(m_status_report_id, APP_STATUS_REPORT_SHORT_INTERVAL, NULL);
            //APP_ERROR_CHECK(err_code);
            
            app_timer_stop(m_sensor_debounce_id);
        }
    } else {
        // Sensor toggle when debouncing not finished
        #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("Sensor toggle when debouncing not finished (m_sensor1_data = %d)", m_sensor1_data);
        #endif
        
        m_app_sensor_is_debouncing = false;
        
        if (m_sensor1_data == 0)
        {
            m_app_sensor_debounce_count = 0;
        } else {
            m_app_sensor_debounce_count = 40;
        }
        
        if (bsp_button_is_pressed(APP_SENSOR))
        {
            bsp_board_led_on(BSP_BOARD_LED_0);
        } else {
            bsp_board_led_off(BSP_BOARD_LED_0);
        }
        
        app_timer_stop(m_sensor_debounce_id);
    }
}


/**@brief Function for handling the status report timeout.
 *
 * @details This function will be called every 5s once the device is paired.
 *
 * @param[in]   p_context   Pointer used for passing some arbitrary information (context) from the
 *                          app_start_timer() call to the timeout handler.
 */
static void status_report_timeout_handler(void * p_context)
{
    ret_code_t err_code;
    
    UNUSED_PARAMETER(p_context);
    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("status_report_timeout_handler. m_message_idx: %d", m_message_idx);
    #endif
    
    app_timer_stop(m_status_report_id);
    
    nrf_drv_wdt_channel_feed(m_wdt_channel_id);
    
    if (m_association.is_associated == true)
    {
        if (m_message_idx % 64 == 0)
        {
            err_code = nrf_drv_saadc_sample();
            APP_ERROR_CHECK(err_code);
           
        }
        // Start advertise for status report, otherwise no action to save power
        advdata_update();
        advertising_start();

        err_code = app_timer_start(m_status_report_id, APP_STATUS_REPORT_INTERVAL, NULL);
        APP_ERROR_CHECK(err_code);
    }
    else
    {
        //app_timer_stop(m_status_report_id);
    }
}


/**@brief Function for handling the Battery measurement timer timeout.
 *
 * @details This function will be called each time the battery level measurement timer expires.
 *
 * @param[in] p_context  Pointer used for passing some arbitrary information (context) from the
 *                       app_start_timer() call to the timeout handler.
 */
static void battery_measure_timeout_handler(void * p_context)
{
    ret_code_t err_code;
    
    UNUSED_PARAMETER(p_context);
    
    err_code = nrf_drv_saadc_sample();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    // Initialize timer module.
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    // Create timers.
    
    err_code = app_timer_create(&m_button_debounce_id,
                                APP_TIMER_MODE_REPEATED,
                                button_debounce_timeout_handler);
    APP_ERROR_CHECK(err_code);
    
    err_code = app_timer_create(&m_sensor_debounce_id,
                                APP_TIMER_MODE_REPEATED,
                                sensor_debounce_timeout_handler);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_status_report_id,
                                APP_TIMER_MODE_REPEATED,
                                status_report_timeout_handler);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_battery_measure_id,
                                APP_TIMER_MODE_REPEATED,
                                battery_measure_timeout_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    if (m_association.is_associated == false)
    {
        err_code = sd_ble_gap_device_name_set(&sec_mode,
                                              (const uint8_t *)CON_DEVICE_NAME,
                                              strlen(CON_DEVICE_NAME));
        APP_ERROR_CHECK(err_code);
    }
    else
    {
        err_code = sd_ble_gap_device_name_set(&sec_mode,
                                              (const uint8_t *)NCON_DEVICE_NAME,
                                              strlen(NCON_DEVICE_NAME));
        APP_ERROR_CHECK(err_code);
    }   

    /* YOUR_JOB: Use an appearance value matching the application's use case.
       err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_);
       APP_ERROR_CHECK(err_code); */

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for handling the YYY Service events.
 * YOUR_JOB implement a service handler function depending on the event the service you are using can generate
 *
 * @details This function will be called for all YY Service events which are passed to
 *          the application.
 *
 * @param[in]   p_yy_service   YY Service structure.
 * @param[in]   p_evt          Event received from the YY Service.
 *
 *
static void on_yys_evt(ble_yy_service_t     * p_yy_service,
                       ble_yy_service_evt_t * p_evt)
{
    switch (p_evt->evt_type)
    {
        case BLE_YY_NAME_EVT_WRITE:
            APPL_LOG("[APPL]: charact written with value %s. ", p_evt->params.char_xx.value.p_str);
            break;

        default:
            // No implementation needed.
            break;
    }
}
*/

/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    ret_code_t         err_code;
    nrf_ble_qwr_init_t qwr_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

    /* YOUR_JOB: Add code to initialize the services used by the application.
       ble_xxs_init_t                     xxs_init;
       ble_yys_init_t                     yys_init;

       // Initialize XXX Service.
       memset(&xxs_init, 0, sizeof(xxs_init));

       xxs_init.evt_handler                = NULL;
       xxs_init.is_xxx_notify_supported    = true;
       xxs_init.ble_xx_initial_value.level = 100;

       err_code = ble_bas_init(&m_xxs, &xxs_init);
       APP_ERROR_CHECK(err_code);

       // Initialize YYY Service.
       memset(&yys_init, 0, sizeof(yys_init));
       yys_init.evt_handler                  = on_yys_evt;
       yys_init.ble_yy_initial_value.counter = 0;

       err_code = ble_yy_service_init(&yys_init, &yy_init);
       APP_ERROR_CHECK(err_code);
     */
}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        NRF_LOG_INFO("BLE_CONN_PARAMS_EVT_FAILED");
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting timers.
 */
static void application_timers_start(void)
{
    /* YOUR_JOB: Start your timers. below is an example of how to start a timer.
       ret_code_t err_code;
       err_code = app_timer_start(m_app_timer_id, TIMER_INTERVAL, NULL);
       APP_ERROR_CHECK(err_code); */
    
    ret_code_t err_code;

    err_code = app_timer_start(m_status_report_id, APP_STATUS_REPORT_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    ret_code_t err_code;

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
/*static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    ret_code_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("BLE_ADV_EVT_FAST: Fast advertising.");
            #endif
        
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_ADV_EVT_IDLE:
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("BLE_ADV_EVT_IDLE: Not advertising.");
            #endif
        
            sleep_mode_enter();
            break;

        default:
            break;
    }
}*/


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code = NRF_SUCCESS;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_DISCONNECTED:
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("BLE_GAP_EVT_DISCONNECTED: Disconnected");
            #endif
            // LED indication will be changed when advertising starts.
            break;

        case BLE_GAP_EVT_CONNECTED:
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("Connected.");
        
            NRF_LOG_INFO("evt.gap_evt.params.connected.peer_addr %X:%X:%X:%X:%X:%X",
                        p_ble_evt->evt.gap_evt.params.connected.peer_addr.addr[5], 
                        p_ble_evt->evt.gap_evt.params.connected.peer_addr.addr[4], 
                        p_ble_evt->evt.gap_evt.params.connected.peer_addr.addr[3], 
                        p_ble_evt->evt.gap_evt.params.connected.peer_addr.addr[2], 
                        p_ble_evt->evt.gap_evt.params.connected.peer_addr.addr[1], 
                        p_ble_evt->evt.gap_evt.params.connected.peer_addr.addr[0]);
            #endif
        
            //err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            //APP_ERROR_CHECK(err_code);
        
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
        
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);
        
        
            memcpy(m_association.ble_gap_addr.addr, p_ble_evt->evt.gap_evt.params.connected.peer_addr.addr, BLE_GAP_ADDR_LEN);
            m_association.is_associated = true;
            
            fstorage_write_associate_address(m_association.ble_gap_addr.addr);
            
            //err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            //APP_ERROR_CHECK(err_code);
            
            /* start advertising status report */
            /*gap_params_init();
            non_connectable_adv_init();
            
            err_code = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_adv_data, &m_adv_params);
            APP_ERROR_CHECK(err_code);
            
            while (m_isAdvertising == true);
            advertising_start();

            application_timers_start();*/
                
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            #ifdef APP_DEBUG_MODE
            NRF_LOG_DEBUG("PHY update request.");
            #endif
            
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            #ifdef APP_DEBUG_MODE
            NRF_LOG_DEBUG("GATT Client Timeout.");
            #endif
        
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            #ifdef APP_DEBUG_MODE
            NRF_LOG_DEBUG("GATT Server Timeout.");
            #endif
        
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for the Peer Manager initialization.
 */
static void peer_manager_init(void)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated when button is pressed.
 */
static void bsp_event_handler(bsp_event_t event)
{
    ret_code_t err_code;

    switch (event)
    {
        case BSP_EVENT_SLEEP:
            sleep_mode_enter();
            break; // BSP_EVENT_SLEEP

        case BSP_EVENT_DISCONNECT:
            NRF_LOG_INFO("BSP_EVENT_DISCONNECT");
            err_code = sd_ble_gap_disconnect(m_conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break; // BSP_EVENT_DISCONNECT

        case BSP_EVENT_WHITELIST_OFF:
            if (m_conn_handle == BLE_CONN_HANDLE_INVALID)
            {
                err_code = ble_advertising_restart_without_whitelist(&m_advertising);
                if (err_code != NRF_ERROR_INVALID_STATE)
                {
                    APP_ERROR_CHECK(err_code);
                }
            }
            break; // BSP_EVENT_WHITELIST_OFF

        case BSP_EVENT_KEY_0:
            m_app_button_debounce_count = 0;
            app_timer_start(m_button_debounce_id, BUTTON_DEBOUNCE_INTERVAL, NULL);
            
            break; // BSP_EVENT_KEY_0

        case BSP_EVENT_KEY_1:
            #ifdef APP_DEBUG_MODE
                NRF_LOG_INFO("Key_1 Change");
            #endif
            if (m_app_sensor_is_debouncing == false)
            {
                //m_app_sensor_debounce_count = 0;
                m_app_sensor_is_debouncing = true;
                
                app_timer_start(m_sensor_debounce_id, SENSOR_DEBOUNCE_INTERVAL, NULL);
            }
            
            break; // BSP_EVENT_KEY_1
        
        default:
            break;
    }
}


/**@brief Function for initializing the connectable advertisement parameters.
 *
 * @details This function initializes the advertisement parameters to values that will put
 *          the application in connectable mode.
 *
 */
static void connectable_adv_init(void)
{
    // Initialize advertising parameters (used when starting advertising).
    memset(&m_adv_params, 0, sizeof(m_adv_params));

    m_adv_params.properties.type = BLE_GAP_ADV_TYPE_CONNECTABLE_SCANNABLE_UNDIRECTED;
    m_adv_params.duration        = APP_CON_ADV_DURATION;

    m_adv_params.p_peer_addr   = NULL;
    m_adv_params.filter_policy = BLE_GAP_ADV_FP_ANY;
    m_adv_params.interval      = APP_CON_ADV_INTERVAL;
    m_adv_params.primary_phy   = BLE_GAP_PHY_1MBPS;
}


/**@brief Function for initializing the non-connectable advertisement parameters.
 *
 * @details This function initializes the advertisement parameters to values that will put
 *          the application in non-connectable mode.
 *
 */
static void non_connectable_adv_init(void)
{
    // Initialize advertising parameters (used when starting advertising).
    memset(&m_adv_params, 0, sizeof(m_adv_params));

    m_adv_params.properties.type = BLE_GAP_ADV_TYPE_NONCONNECTABLE_NONSCANNABLE_UNDIRECTED; // BLE_GAP_ADV_TYPE_NONCONNECTABLE_SCANNABLE_DIRECTED; //
    m_adv_params.duration        = APP_NCON_ADV_DURATION;
    
    m_adv_params.p_peer_addr     = NULL;
    m_adv_params.filter_policy   = BLE_GAP_ADV_FP_ANY;
    m_adv_params.interval        = APP_NCON_ADV_INTERVAL;
    m_adv_params.primary_phy     = BLE_GAP_PHY_1MBPS;
}


/** @brief Function for updating advertising data
 */
static void advdata_update(void)
{
    //ret_code_t err_code;
    
    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("m_adv_handle: %d", m_adv_handle);
    #endif
    
    //err_code = sd_ble_gap_adv_stop(m_adv_handle);
    //APP_ERROR_CHECK(err_code);
    
    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("m_message_idx: %d", m_message_idx);
    #endif
    
    if (m_association.is_associated == true)
    {
        bsp_board_led_invert(BSP_BOARD_LED_1);   //just to provide a visual indication
        m_enc_advdata[7] = m_message_idx >> 8;
        m_enc_advdata[8] = m_message_idx % 256;
        m_enc_advdata[9] = m_battery_level >> 8;
        m_enc_advdata[10] = m_battery_level % 256;
        m_enc_advdata[11] = m_sensor1_data >> 8;
        m_enc_advdata[12] = m_sensor1_data % 256;
            
        m_adv_data.adv_data.p_data = m_enc_advdata;
        
        m_message_idx++;
    }
    else
    {
        memcpy(&m_enc_advdata[7], SSHSS_CONNECTABLE_ADV_MANUF_DATA, ADV_ADDL_MANUF_DATA_LEN);
        
        m_adv_data.adv_data.p_data = m_enc_advdata;
    }
    
    
}


/**@brief Software interrupt 1 IRQ Handler, handles radio notification interrupts.
*/
void SWI1_IRQHandler(bool radio_evt)
{
    if (radio_evt)
    {
        /*if (m_association.is_associated == true)
        {
            bsp_board_led_invert(BSP_BOARD_LED_1);   //just to provide a visual indication
            m_enc_advdata[7] = m_message_idx >> 8;
            m_enc_advdata[8] = m_message_idx % 256;
            m_enc_advdata[9] = m_battery_level >> 8;
            m_enc_advdata[10] = m_battery_level % 256;
            m_enc_advdata[11] = m_sensor1_data >> 8;
            m_enc_advdata[12] = m_sensor1_data % 256;
            
            m_adv_data.adv_data.p_data = m_enc_advdata;
        }
        else
        {
            memcpy(&m_enc_advdata[7], SSHSS_CONNECTABLE_ADV_MANUF_DATA, ADV_ADDL_MANUF_DATA_LEN);
        }*/
        
        m_isAdvertising = false;
    }
}


/**@brief Function for initializing Radio Notification Software Interrupts.
 */
uint32_t radio_notification_init(uint32_t irq_priority, uint8_t notification_type, uint8_t notification_distance)
{
    uint32_t err_code;

    err_code = sd_nvic_ClearPendingIRQ(SWI1_IRQn);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = sd_nvic_SetPriority(SWI1_IRQn, irq_priority);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = sd_nvic_EnableIRQ(SWI1_IRQn);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Configure the event
    err_code = sd_radio_notification_cfg_set(notification_type, notification_distance);
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_INFO("radio_notification_init: Skipping First Burn Error");
        return NRF_SUCCESS;
    }
    
    return NRF_SUCCESS;
}



/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    ret_code_t                  err_code;
    
    ble_advdata_t               advdata;
    ble_advdata_manuf_data_t    manuf_data;
    
    // Build and set advertising data
    memset(&advdata, 0, sizeof(advdata));
    
    manuf_data.company_identifier   = COMPANY_IDENTIFIER;
    manuf_data.data.size            = ADV_ADDL_MANUF_DATA_LEN;
    manuf_data.data.p_data          = m_addl_adv_manuf_data;
    
    advdata.name_type               = BLE_ADVDATA_SHORT_NAME;
    advdata.short_name_len          = 2;
    advdata.include_appearance      = false;
    advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    //advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    //advdata.uuids_complete.p_uuids  = m_adv_uuids;
    advdata.p_manuf_specific_data   = &manuf_data;
    
    err_code = ble_advdata_encode(&advdata, m_adv_data.adv_data.p_data, &m_adv_data.adv_data.len);
    APP_ERROR_CHECK(err_code);
    
    memcpy(m_enc_advdata, m_adv_data.adv_data.p_data, m_adv_data.adv_data.len);
    
    
    err_code = radio_notification_init(3, NRF_RADIO_NOTIFICATION_TYPE_INT_ON_INACTIVE, NRF_RADIO_NOTIFICATION_DISTANCE_NONE);
    APP_ERROR_CHECK(err_code);
    
    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("ADV_ADDL_MANUF_DATA_LEN: %d", ADV_ADDL_MANUF_DATA_LEN);
    #endif
    
    /*ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;

    init.evt_handler = on_adv_evt;*/

    //err_code = ble_advertising_init(&m_advertising, &init);
    //APP_ERROR_CHECK(err_code);

    //ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
    
    if(m_association.is_associated == false)
    {
        err_code = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_adv_data, &m_adv_params);
        APP_ERROR_CHECK(err_code);
    }
    else
    {
        err_code = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_adv_data, &m_adv_params);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
//static void buttons_leds_init(bool * p_erase_bonds)
static void buttons_leds_init(void)
{
    ret_code_t err_code;
    //bsp_event_t startup_event;

    err_code = bsp_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);
    
    bsp_event_to_button_action_assign(BSP_BOARD_BUTTON_1, BSP_BUTTON_ACTION_RELEASE, BSP_EVENT_KEY_1);
    APP_ERROR_CHECK(err_code);

    //err_code = bsp_btn_ble_init(NULL, &startup_event);
    //APP_ERROR_CHECK(err_code);

    //*p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}


/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing watchdog.
 */
static void wdt_init(void)
{
    ret_code_t err_code;
    
    nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
    
    err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
    APP_ERROR_CHECK(err_code);
    
    err_code = nrf_drv_wdt_channel_alloc(&m_wdt_channel_id);
    APP_ERROR_CHECK(err_code);
}


/*
 *@brief Function for initializing fstorage.
 */
static void fstorage_init(void)
{
    ret_code_t err_code;
    
    nrf_fstorage_api_t* p_fs_api;
   
    p_fs_api = &nrf_fstorage_sd;
    
    err_code = nrf_fstorage_init(&fstorage, p_fs_api, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}


/**@brief Function for starting application timers.
 */
static void watchdog_start(void)
{
    // Start watchdog timers.
    nrf_drv_wdt_enable();
}


/**@brief Function for starting advertising.
 */
/*static void advertising_start(bool erase_bonds)
{
    if (erase_bonds == true)
    {
        delete_bonds();
        // Advertising is started by PM_EVT_PEERS_DELETED_SUCEEDED event
    }
    else
    {
        ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);

        APP_ERROR_CHECK(err_code);
    }
}*/
/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{
    ret_code_t err_code;
    
    err_code = sd_ble_gap_adv_start(m_adv_handle, APP_BLE_CONN_CFG_TAG);
    APP_ERROR_CHECK(err_code);
    
    m_isAdvertising = true;
}


/**@brief Function to align data length to flash memory when writing in.
  */
static uint32_t fstorage_len_modifier(uint32_t len)
{
    if (len % sizeof(uint32_t))
    {
        return (len + sizeof(uint32_t) - (len % sizeof(uint32_t)));
    }

    return len;
}


/*
 *@brief Function to read association central address.
 */
static void fstorage_read_associate_address(uint8_t* asso_addr)
{
    ret_code_t err_code;
    
    uint32_t flash_asso_addr = FLASH_ASSOCIATE_INFO_START;
    
    err_code = nrf_fstorage_read(&fstorage, flash_asso_addr, asso_addr, BLE_GAP_ADDR_LEN);

    if (err_code != NRF_SUCCESS)
    {
        #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("fstorage_read_associate_address failed.");
        #endif
        
        return;
    }
    
    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("fstorage_read_associate_address.");
    #endif
}

static uint32_t asso_addr_u32[2];
/*
 *@brief Function to write association central address.
 */
static void fstorage_write_associate_address(uint8_t* asso_addr)
{
    ret_code_t err_code;

    uint32_t flash_asso_addr = FLASH_ASSOCIATE_INFO_START;

    asso_addr_u32[1] = (asso_addr[5] << 8) + asso_addr[4];
    asso_addr_u32[0] = (((((asso_addr[3] << 8) + asso_addr[2]) << 8) + asso_addr[1]) << 8) + asso_addr[0];

    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("fstorage_write_associate_address: %X:%X:%X:%X:%X:%X",
        asso_addr[5],
        asso_addr[4],
        asso_addr[3],
        asso_addr[2],
        asso_addr[1],
        asso_addr[0]);
    #endif

    err_code = nrf_fstorage_write(&fstorage, flash_asso_addr, &asso_addr_u32, sizeof(asso_addr_u32), NULL);
    
    if (err_code != NRF_SUCCESS)
    {
        #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("fstorage_write_associate_address failed.");
        #endif
        
        return;
    }
    
    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("fstorage_write_associate_address.");
    #endif
}


/*
 *@brief Function to delete association central address.
 */
static void fstorage_delete_associate_address()
{
    ret_code_t err_code;
    
    err_code = nrf_fstorage_erase(&fstorage, FLASH_ASSOCIATE_INFO_START, 1, NULL);
    if (err_code != NRF_SUCCESS)
    {
        #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("fstorage_delete_associate_address failed.");
        #endif
        
        return;
    }

}


/**@brief Function for application main entry.
 */
int main(void)
{
    ret_code_t err_code;
    //bool erase_bonds;

    // Initialize.
    log_init();
    wdt_init();
    timers_init();
    fstorage_init();
    
    buttons_leds_init();
    
    power_management_init();
    
    m_association_mode = bsp_board_button_state_get(APP_BUTTON);
    
    fstorage_read_associate_address(m_association.ble_gap_addr.addr);
    #ifdef APP_DEBUG_MODE
    NRF_LOG_INFO("fstorage_read_associate_address %X:%X:%X:%X:%X:%X",
        m_association.ble_gap_addr.addr[5], 
        m_association.ble_gap_addr.addr[4], 
        m_association.ble_gap_addr.addr[3], 
        m_association.ble_gap_addr.addr[2], 
        m_association.ble_gap_addr.addr[1], 
        m_association.ble_gap_addr.addr[0]);
    #endif
    
    if (memcmp(m_association.ble_gap_addr.addr, EMPTY_ASSOCIATE_ADDRESS, sizeof(m_association.ble_gap_addr.addr)) != 0)
    {
        m_association.is_associated = true;
        
        if (m_association_mode)
        {
            fstorage_delete_associate_address();
            
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("Association Erased.");
            #endif
            
            nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
        }
    }
    else
    {
        m_association.is_associated = false;
        
        if (!m_association_mode)
        {
            #ifdef APP_DEBUG_MODE
            NRF_LOG_INFO("Not Associated...System Going to Sleep.");
            #endif
            
            // The startup was not because of button presses. This is the first start.
            // Go into System-Off mode. Button presses will wake the chip up.
            nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
        }
    }
    
    // Reach this point means:
    // 1. the device is associated or 
    // 2. the device is not asocaited and the application is woken up by pressing APP_BUTTON.
    
    // Continue initialization
    adc_configure();
    
    ble_stack_init();
    gap_params_init();
    gatt_init();
    
    services_init();
    conn_params_init();
    peer_manager_init();
    
    if (m_association.is_associated == false)
    {
        connectable_adv_init();
        
        memcpy(&m_enc_advdata[7], SSHSS_CONNECTABLE_ADV_MANUF_DATA, ADV_ADDL_MANUF_DATA_LEN);
        m_adv_data.adv_data.p_data = m_enc_advdata;
    }
    else
    {
        non_connectable_adv_init();
    }
    
    advertising_init();
    
    // Start execution.
    #ifdef APP_DEBUG_MODE
        NRF_LOG_INFO("Template example started.");
    #endif
    
    watchdog_start();
    if (m_association.is_associated == true)
    {
        application_timers_start();
        
        err_code = nrf_drv_saadc_sample();
        APP_ERROR_CHECK(err_code);
    }
    
    advdata_update();
    advertising_start();
    
    // Enter main loop.
    for (;;)
    {
        idle_state_handle();
    }
}


/**
 * @}
 */
