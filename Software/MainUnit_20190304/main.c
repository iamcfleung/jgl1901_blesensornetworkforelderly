/**
 * Copyright (c) 2016 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/**
 * @brief Application main file for the BLE multirole LE Secure Connections (LESC) example.
 *
 * @detail This application demonstrates bonding with LE Secure Connections both as a peripheral and as a central.
 *
 * LED layout:
 * LED 1: Central side is scanning.       LED 2: Central side is connected to a peripheral.
 * LED 3: Peripheral side is advertising. LED 4: Peripheral side is connected to a central.
 *
 * @note: This application requires the use of an external ECC library for public key and shared secret calculation.
 *        Refer to the application's documentation for more details.
 *
 */

#include "sdk_config.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_fstorage.h"
#include "nrf_fstorage_sd.h"
#include "peer_manager.h"
#include "peer_manager_handler.h"
#include "app_timer.h"
#include "bsp_btn_ble.h"
#include "ble.h"
#include "app_util.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "ble_db_discovery.h"
// TL //#include "ble_hrs.h"
// TL //#include "ble_hrs_c.h"
#include "ble_bas_c.h"
#include "ble_lbs_c.h"
#include "ble_conn_state.h"
//#include "fds.h"
#include "nrf_crypto.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_lesc.h"
#include "nrf_ble_qwr.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_ble_scan.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"



#define DEVICE_NAME                     "SSHSS MU"                               /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME               "Gleen Co Ltd"                                    /**< Manufacturer. Will be passed to Device Information Service. */

#define LESC_DEBUG_MODE                 0                                               /**< Set to 1 to use the LESC debug keys. The debug mode allows you to use a sniffer to inspect traffic. */
#define LESC_MITM_NC                    1                                               /**< Use MITM (Numeric Comparison). */

/** @brief The maximum number of peripheral and central links combined. */
#define NRF_BLE_LINK_COUNT              (NRF_SDH_BLE_PERIPHERAL_LINK_COUNT + NRF_SDH_BLE_CENTRAL_LINK_COUNT)


// TL //

#define FLASH_MAX_PERIPHERAL            128
//#define FLASH_PERIPHERAL_COUNT          0x80000
#define FLASH_PERIPHERAL_ADDR_START     0x80000
#define FLASH_PERIPHERAL_ADDR_END       0x80fff

// TL //

#define APP_BLE_CONN_CFG_TAG            1                                               /**< Tag that identifies the SoftDevice BLE configuration. */

#define CENTRAL_SCANNING_LED            BSP_BOARD_LED_0
#define CENTRAL_CONNECTED_LED           BSP_BOARD_LED_1
#define LEDBUTTON_LED                   BSP_BOARD_LED_2                                 /**< LED to indicate a change of state of the Button characteristic on the peer. */
//#define PERIPHERAL_ADVERTISING_LED      BSP_BOARD_LED_2
#define PERIPHERAL_CONNECTED_LED        BSP_BOARD_LED_3

#define SCAN_DURATION                   0x0000                                          /**< Duration of the scanning in units of 10 milliseconds. If set to 0x0000, scanning continues until it is explicitly disabled. */
#define APP_ADV_DURATION                18000                                           /**< The advertising duration (180 seconds) in units of 10 milliseconds. */


#define SEC_PARAMS_BOND                 1                                               /**< Perform bonding. */
#if LESC_MITM_NC
#define SEC_PARAMS_MITM                 1                                               /**< Man In The Middle protection required. */
#define SEC_PARAMS_IO_CAPABILITIES      BLE_GAP_IO_CAPS_DISPLAY_YESNO                   /**< Display Yes/No to force Numeric Comparison. */
#else
#define SEC_PARAMS_MITM                 0                                               /**< Man In The Middle protection required. */
#define SEC_PARAMS_IO_CAPABILITIES      BLE_GAP_IO_CAPS_NONE                            /**< No I/O caps. */
#endif
#define SEC_PARAMS_LESC                 1                                               /**< LE Secure Connections pairing required. */
#define SEC_PARAMS_KEYPRESS             0                                               /**< Keypress notifications not required. */
#define SEC_PARAMS_OOB                  0                                               /**< Out Of Band data not available. */
#define SEC_PARAMS_MIN_KEY_SIZE         7                                               /**< Minimum encryption key size in octets. */
#define SEC_PARAMS_MAX_KEY_SIZE         16                                              /**< Maximum encryption key size in octets. */

#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                           /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                          /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                               /**< Number of attempts before giving up the connection parameter negotiation. */

/**@brief   Priority of the application BLE event handler.
 * @note    There is no need to modify this value.
 */
#define APP_BLE_OBSERVER_PRIO           3


typedef struct
{
    bool           is_connected;
    ble_gap_addr_t address;
} conn_peer_t;


// TL //BLE_HRS_DEF(m_hrs);                                                         /**< Heart Rate Service instance. */
// TL //BLE_HRS_C_DEF(m_hrs_c);                                                     /**< Structure used to identify the Heart Rate client module. */
BLE_BAS_C_DEF(m_bas_c);                                                     /**< Structure used to identify the Battery Service client module. */
NRF_BLE_GATT_DEF(m_gatt);                                                   /**< GATT module instance. */
NRF_BLE_QWRS_DEF(m_qwr, NRF_SDH_BLE_TOTAL_LINK_COUNT);                      /**< Context for the Queued Write module.*/
// TL //BLE_ADVERTISING_DEF(m_advertising);                                         /**< Advertising module instance. */
BLE_LBS_C_ARRAY_DEF(m_lbs_c, NRF_SDH_BLE_CENTRAL_LINK_COUNT);               /**< LED button client instances. */
BLE_DB_DISCOVERY_ARRAY_DEF(m_db_disc, NRF_SDH_BLE_CENTRAL_LINK_COUNT);      /**< Database discovery module instances. */
//BLE_DB_DISCOVERY_DEF(m_db_disc);                                            /**< Database discovery module instance. */
NRF_BLE_SCAN_DEF(m_scan);                                                   /**< Scanning Module instance. */

static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt);

//static uint16_t           m_conn_handle_hrs_c                = BLE_CONN_HANDLE_INVALID;  /**< Connection handle for the HRS central application. */
static volatile uint16_t  m_conn_handle_num_comp_central     = BLE_CONN_HANDLE_INVALID;  /**< Connection handle for the central that needs a numeric comparison button press. */
static volatile uint16_t  m_conn_handle_num_comp_peripheral  = BLE_CONN_HANDLE_INVALID;  /**< Connection handle for the peripheral that needs a numeric comparison button press. */

static conn_peer_t        m_connected_peers[NRF_BLE_LINK_COUNT];                         /**< Array of connected peers. */

static uint8_t            m_bonded_peripheral_count = 0;
static ble_gap_addr_t     m_bonded_peripheral[FLASH_MAX_PERIPHERAL];
static const uint8_t      INVALID_BONDED_ADDRESS[BLE_GAP_ADDR_LEN] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

static char * roles_str[] =
{
    "INVALID_ROLE",
    "PERIPHERAL",
    "CENTRAL",
};


static void fstorage_add_peripheral(uint8_t* peri_count, uint8_t* peri_addr);


/**@brief Names that the central application scans for, and that are advertised by the peripherals.
 *  If these are set to empty strings, the UUIDs defined below are used.
 */
static const char m_target_periph_name[] = "";

NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
{
    /* Set a handler for fstorage events. */
    .evt_handler = fstorage_evt_handler,

    /* These below are the boundaries of the flash space assigned to this instance of fstorage.
     * You must set these manually, even at runtime, before nrf_fstorage_init() is called.
     * The function nrf5_flash_end_addr_get() can be used to retrieve the last address on the
     * last page of flash available to write data. */
    .start_addr = FLASH_PERIPHERAL_ADDR_START,
    .end_addr   = FLASH_PERIPHERAL_ADDR_END,
};

static uint32_t fstorage_len_modifier(uint32_t len)
{
    if (len % sizeof(uint32_t))
    {
        return (len + sizeof(uint32_t) - (len % sizeof(uint32_t)));
    }

    return len;
}


/**@brief UUIDs that the central application scans for if the name above is set to an empty string,
 * and that are to be advertised by the peripherals.
 */
/* TL
static ble_uuid_t m_adv_uuids[] = {{BLE_UUID_HEART_RATE_SERVICE,         BLE_UUID_TYPE_BLE},
                                   {BLE_UUID_RUNNING_SPEED_AND_CADENCE,  BLE_UUID_TYPE_BLE}};
*/
                                   
/**@brief Function for handling asserts in the SoftDevice.
 *
 * @details This function is called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and is not meant for the final product. You need to analyze
 *          how your product is supposed to react in case of assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num     Line number of the failing assert call.
 * @param[in] p_file_name  File name of the failing assert call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(0xDEADBEEF, line_num, p_file_name);
}


/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code that contains information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for handling Scanning Module events.
 */
static void scan_evt_handler(scan_evt_t const * p_scan_evt)
{
    ret_code_t err_code;

    switch(p_scan_evt->scan_evt_id)
    {
        case NRF_BLE_SCAN_EVT_CONNECTING_ERROR:
        {
            err_code = p_scan_evt->params.connecting_err.err_code;
            APP_ERROR_CHECK(err_code);
        } break;

        default:
            break;
    }
}


/**@brief Function for initializing the scanning and setting the filters.
 */
static void scan_init(void)
{
    ret_code_t          err_code;
    ble_uuid_t          target_uuid = 
    {
        .uuid = BLE_UUID_BATTERY_SERVICE,
        .type = BLE_UUID_TYPE_BLE
    };
    nrf_ble_scan_init_t init_scan;

    memset(&init_scan, 0, sizeof(init_scan));

    init_scan.connect_if_match = true;
    init_scan.conn_cfg_tag     = APP_BLE_CONN_CFG_TAG;

    err_code = nrf_ble_scan_init(&m_scan, &init_scan, scan_evt_handler);
    APP_ERROR_CHECK(err_code);

    if (strlen(m_target_periph_name) != 0)
    {
        err_code = nrf_ble_scan_filter_set(&m_scan, 
                                           SCAN_NAME_FILTER, 
                                           m_target_periph_name);
        APP_ERROR_CHECK(err_code);
    }

    err_code = nrf_ble_scan_filter_set(&m_scan, 
                                       SCAN_UUID_FILTER, 
                                       &target_uuid);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_scan_filters_enable(&m_scan, 
                                           NRF_BLE_SCAN_NAME_FILTER | NRF_BLE_SCAN_UUID_FILTER, 
                                           false);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the scanning.
 */
static void scan_start(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_scan_start(&m_scan);
    APP_ERROR_CHECK(err_code);
    
    // Turn on the LED to signal scanning.
    bsp_board_led_on(CENTRAL_SCANNING_LED);

    NRF_LOG_INFO("Scanning");
}


/**@brief Function for initializing the advertising and the scanning.
 */
/* TL
static void adv_scan_start(void)
{
    ret_code_t err_code;

    scan_start();

    // Turn on the LED to signal scanning.
    bsp_board_led_on(CENTRAL_SCANNING_LED);

    // Start advertising.
    err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_INFO("Advertising");
}
*/

/**@brief Handles events coming from the LED Button central module.
 *
 * @param[in] p_lbs_c     The instance of LBS_C that triggered the event.
 * @param[in] p_lbs_c_evt The LBS_C event.
 */
static void lbs_c_evt_handler(ble_lbs_c_t * p_lbs_c, ble_lbs_c_evt_t * p_lbs_c_evt)
{
    switch (p_lbs_c_evt->evt_type)
    {
        case BLE_LBS_C_EVT_DISCOVERY_COMPLETE:
        {
            ret_code_t err_code;

            NRF_LOG_INFO("LED Button Service discovered on conn_handle 0x%x",
                         p_lbs_c_evt->conn_handle);

            err_code = app_button_enable();
            APP_ERROR_CHECK(err_code);

            // LED Button Service discovered. Enable notification of Button.
            err_code = ble_lbs_c_button_notif_enable(p_lbs_c);
            APP_ERROR_CHECK(err_code);
        } break; // BLE_LBS_C_EVT_DISCOVERY_COMPLETE

        case BLE_LBS_C_EVT_BUTTON_NOTIFICATION:
        {
            NRF_LOG_INFO("Link 0x%x, Button state changed on peer to 0x%x",
                         p_lbs_c_evt->conn_handle,
                         p_lbs_c_evt->params.button.button_state);

            if (p_lbs_c_evt->params.button.button_state)
            {
                bsp_board_led_on(LEDBUTTON_LED);
            }
            else
            {
                bsp_board_led_off(LEDBUTTON_LED);
            }
        } break; // BLE_LBS_C_EVT_BUTTON_NOTIFICATION

        default:
            // No implementation needed.
            break;
    }
}


static bool peri_bonded(ble_gap_addr_t const * peri_addr)
{
    for (uint32_t i = 0; i < m_bonded_peripheral_count; i++)
    {
        if (memcmp(peri_addr->addr,
            m_bonded_peripheral[i].addr,
            sizeof(m_bonded_peripheral[i].addr)) == 0)
        {
            return true;
        }
    }
    return false;
}

/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    pm_handler_on_pm_evt(p_evt);
    pm_handler_disconnect_on_sec_failure(p_evt);
    pm_handler_flash_clean(p_evt);

    switch (p_evt->evt_id)
    {
        case PM_EVT_BONDED_PEER_CONNECTED:
        {
            NRF_LOG_INFO("PM_EVT_BONDED_PEER_CONNECTED");
        }
        
        case PM_EVT_CONN_SEC_SUCCEEDED :
        {
            NRF_LOG_INFO("PM_EVT_CONN_SEC_SUCCEEDED ");
            if (p_evt->params.conn_sec_start.procedure == PM_CONN_SEC_PROCEDURE_ENCRYPTION)
            {
                //NRF_LOG_INFO("ENCRYPTION !!!!!!!!!!!!");
            }
            else if (p_evt->params.conn_sec_start.procedure == PM_CONN_SEC_PROCEDURE_BONDING)
            {
                if (peri_bonded(&m_connected_peers[p_evt->conn_handle].address) == false)
                {
                    memcpy(&m_bonded_peripheral[m_bonded_peripheral_count].addr, m_connected_peers[p_evt->conn_handle].address.addr, sizeof(m_connected_peers[p_evt->conn_handle].address.addr));
                    
                    //Adding bonding address to flash
                    fstorage_add_peripheral(&m_bonded_peripheral_count, m_connected_peers[p_evt->conn_handle].address.addr);
                    //NRF_LOG_INFO("sizeof(m_connected_peers[p_evt->conn_handle].address.addr): %d", sizeof(m_connected_peers[p_evt->conn_handle].address.addr));
                    
                }
                    
            }
        }
        
        case PM_EVT_PEERS_DELETE_SUCCEEDED:
            // TL //adv_scan_start();
            scan_start();
            break;
        
        case PM_EVT_CONN_SEC_FAILED:
        {
            /* Often, when securing fails, it shouldn't be restarted, for security reasons.
             * Other times, it can be restarted directly.
             * Sometimes it can be restarted, but only after changing some Security Parameters.
             * Sometimes, it cannot be restarted until the link is disconnected and reconnected.
             * Sometimes it is impossible, to secure the link, or the peer device does not support it.
             * How to handle this error is highly application dependent. */
            NRF_LOG_INFO("PM_EVT_CONN_SEC_FAILED");
        } break;
 
        case PM_EVT_CONN_SEC_CONFIG_REQ:
        {
            // Reject pairing request from an already bonded peer.
            // TL //
            //pm_conn_sec_config_t conn_sec_config = {.allow_repairing = false};
            // TL //
            pm_conn_sec_config_t conn_sec_config = {.allow_repairing = true};
            pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
        } break;
        
        default:
            break;
    }
}


/**@brief Function for changing filter settings after establishing the connection.
 */
/* TL
static void filter_settings_change(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_scan_all_filter_remove(&m_scan);
    APP_ERROR_CHECK(err_code);

    if (strlen(m_target_periph_name) != 0)
    {
        err_code = nrf_ble_scan_filter_set(&m_scan, 
                                           SCAN_NAME_FILTER, 
                                           m_target_periph_name);
        APP_ERROR_CHECK(err_code);
    }
}
*/

/**@brief Handles events coming from the Heart Rate central module.
 */
/* TL
static void hrs_c_evt_handler(ble_hrs_c_t * p_hrs_c, ble_hrs_c_evt_t * p_hrs_c_evt)
{
    switch (p_hrs_c_evt->evt_type)
    {
        case BLE_HRS_C_EVT_DISCOVERY_COMPLETE:
        {
            if (m_conn_handle_hrs_c == BLE_CONN_HANDLE_INVALID)
            {
                ret_code_t err_code;

                m_conn_handle_hrs_c = p_hrs_c_evt->conn_handle;

                // We do not want to connect to two peripherals offering the same service, so when
                // a UUID is matched, we check whether we are not already connected to a peer which
                // offers the same service
                filter_settings_change();

                NRF_LOG_INFO("CENTRAL: HRS discovered on conn_handle 0x%x",
                             m_conn_handle_hrs_c);

                err_code = ble_hrs_c_handles_assign(p_hrs_c,
                                                    m_conn_handle_hrs_c,
                                                    &p_hrs_c_evt->params.peer_db);
                APP_ERROR_CHECK(err_code);

                // Heart rate service discovered. Enable notification of Heart Rate Measurement.
                err_code = ble_hrs_c_hrm_notif_enable(p_hrs_c);
                APP_ERROR_CHECK(err_code);
            }
        } break; // BLE_HRS_C_EVT_DISCOVERY_COMPLETE

        case BLE_HRS_C_EVT_HRM_NOTIFICATION:
        {
            NRF_LOG_INFO("CENTRAL: Heart Rate = %d", p_hrs_c_evt->params.hrm.hr_value);
        } break;

        default:
            // No implementation needed.
            break;
    }
}
*/

/**@brief Function for checking whether a link already exists with a newly connected peer.
 *
 * @details This function checks whether the newly connected device is already connected.
 *
 * @param[in]   p_connected_evt Bluetooth connected event.
 * @return                      True if the peer's address is found in the list of connected peers,
 *                              false otherwise.
 */
/* TL
static bool is_already_connected(ble_gap_addr_t const * p_connected_adr)
{
    for (uint32_t i = 0; i < NRF_BLE_LINK_COUNT; i++)
    {
        if (m_connected_peers[i].is_connected)
        {
            if (m_connected_peers[i].address.addr_type == p_connected_adr->addr_type)
            {
                if (memcmp(m_connected_peers[i].address.addr,
                           p_connected_adr->addr,
                           sizeof(m_connected_peers[i].address.addr)) == 0)
                {
                    return true;
                }
            }
        }
    }
    return false;
}
*/





/** @brief Function for handling a numeric comparison match request. */
static void on_match_request(uint16_t conn_handle, uint8_t role)
{
    // Mark the appropriate conn_handle as pending. The rest is handled on button press.
    NRF_LOG_INFO("Press Button 1 to confirm, Button 2 to reject");
    /* TL
    if (role == BLE_GAP_ROLE_CENTRAL)
    {
        m_conn_handle_num_comp_central = conn_handle;
    }
    else if (role == BLE_GAP_ROLE_PERIPH)
    {
        m_conn_handle_num_comp_peripheral = conn_handle;
    }
    */
    m_conn_handle_num_comp_central = conn_handle;
}





/**@brief Function for assigning new connection handle to the available instance of QWR module.
 *
 * @param[in] conn_handle New connection handle.
 */
static void multi_qwr_conn_handle_assign(uint16_t conn_handle)
{
    for (uint32_t i = 0; i < NRF_BLE_LINK_COUNT; i++)
    {
        if (m_qwr[i].conn_handle == BLE_CONN_HANDLE_INVALID)
        {
            ret_code_t err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr[i], conn_handle);
            APP_ERROR_CHECK(err_code);
            break;
        }
    }
}


/**@brief Function for handling BLE Stack events that are common to both the central and peripheral roles.
 * @param[in] conn_handle Connection Handle.
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void on_ble_evt(uint16_t conn_handle, ble_evt_t const * p_ble_evt)
{
    char        passkey[BLE_GAP_PASSKEY_LEN + 1];
    uint16_t    role = ble_conn_state_role(conn_handle);

    pm_handler_secure_on_connection(p_ble_evt);

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            //m_connected_peers[conn_handle].is_connected = true;
            m_connected_peers[conn_handle].address = p_ble_evt->evt.gap_evt.params.connected.peer_addr;
            multi_qwr_conn_handle_assign(conn_handle);
        
            NRF_LOG_INFO("m_connected_peers[].address %X:%X:%X:%X:%X:%X AddrType:0x0%d",
                        m_connected_peers[conn_handle].address.addr[5], 
                        m_connected_peers[conn_handle].address.addr[4], 
                        m_connected_peers[conn_handle].address.addr[3], 
                        m_connected_peers[conn_handle].address.addr[2], 
                        m_connected_peers[conn_handle].address.addr[1], 
                        m_connected_peers[conn_handle].address.addr[0]);
        
            NRF_LOG_INFO("conn_handle: %X", conn_handle);
            /*
            NRF_LOG_INFO("%X", NRF_FICR->DEVICEID[0]);
            NRF_LOG_INFO("%X", NRF_FICR->DEVICEID[1]);*/
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            memset(&m_connected_peers[conn_handle], 0x00, sizeof(m_connected_peers[0]));
            break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            NRF_LOG_INFO("%s: BLE_GAP_EVT_SEC_PARAMS_REQUEST", nrf_log_push(roles_str[role]));
            break;

        case BLE_GAP_EVT_PASSKEY_DISPLAY:
            memcpy(passkey, p_ble_evt->evt.gap_evt.params.passkey_display.passkey, BLE_GAP_PASSKEY_LEN);
            passkey[BLE_GAP_PASSKEY_LEN] = 0x00;
            NRF_LOG_INFO("%s: BLE_GAP_EVT_PASSKEY_DISPLAY: passkey=%s match_req=%d",
                         nrf_log_push(roles_str[role]),
                         nrf_log_push(passkey),
                         p_ble_evt->evt.gap_evt.params.passkey_display.match_request);

            if (p_ble_evt->evt.gap_evt.params.passkey_display.match_request)
            {
                on_match_request(conn_handle, role);
            }
            break;

        case BLE_GAP_EVT_AUTH_KEY_REQUEST:
            NRF_LOG_INFO("%s: BLE_GAP_EVT_AUTH_KEY_REQUEST", nrf_log_push(roles_str[role]));
            break;

        case BLE_GAP_EVT_LESC_DHKEY_REQUEST:
            NRF_LOG_INFO("%s: BLE_GAP_EVT_LESC_DHKEY_REQUEST", nrf_log_push(roles_str[role]));
            break;

         case BLE_GAP_EVT_AUTH_STATUS:
             NRF_LOG_INFO("%s: BLE_GAP_EVT_AUTH_STATUS: status=0x%x bond=0x%x lv4: %d kdist_own:0x%x kdist_peer:0x%x",
                          nrf_log_push(roles_str[role]),
                          p_ble_evt->evt.gap_evt.params.auth_status.auth_status,
                          p_ble_evt->evt.gap_evt.params.auth_status.bonded,
                          p_ble_evt->evt.gap_evt.params.auth_status.sm1_levels.lv4,
                          *((uint8_t *)&p_ble_evt->evt.gap_evt.params.auth_status.kdist_own),
                          *((uint8_t *)&p_ble_evt->evt.gap_evt.params.auth_status.kdist_peer));
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            ret_code_t err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for handling BLE Stack events that are related to central application.
 *
 * @details This function keeps the connection handles of central application up-to-date. It
 * parses scanning reports, initiates a connection attempt to peripherals when a target UUID
 * is found, and manages connection parameter update requests. Additionally, it updates the status
 * of LEDs used to report the central application's activity.
 *
 * @note        Since this function updates connection handles, @ref BLE_GAP_EVT_DISCONNECTED events
 *              must be dispatched to the target application before invoking this function.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void on_ble_central_evt(ble_evt_t const * p_ble_evt)
{
    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;
    ret_code_t            err_code;

    switch (p_ble_evt->header.evt_id)
    {
        // Upon connection, check which peripheral is connected (HR or RSC), initiate DB
        //  discovery, update LEDs status, and resume scanning, if necessary.
        case BLE_GAP_EVT_CONNECTED:
        {
            NRF_LOG_INFO("CENTRAL: Connected, handle: %d.", p_gap_evt->conn_handle);
            // If no Heart Rate Sensor is currently connected, try to find them on this peripheral.
            /* TL
            if (m_conn_handle_hrs_c == BLE_CONN_HANDLE_INVALID)
            {
                NRF_LOG_INFO("CENTRAL: Searching for HRS on conn_handle 0x%x", p_gap_evt->conn_handle);

                err_code = ble_db_discovery_start(&m_db_disc, p_gap_evt->conn_handle);
                APP_ERROR_CHECK(err_code);
            }*/
            err_code = ble_lbs_c_handles_assign(&m_lbs_c[p_gap_evt->conn_handle],
                p_gap_evt->conn_handle,
                NULL);
            APP_ERROR_CHECK(err_code);

            // TL //
            err_code = ble_db_discovery_start(&m_db_disc[p_gap_evt->conn_handle], p_ble_evt->evt.gap_evt.conn_handle);
            APP_ERROR_CHECK(err_code);
            
            // Update LEDs status and check whether it is needed to look for more
            // peripherals to connect to.
            bsp_board_led_on(CENTRAL_CONNECTED_LED);
            if (ble_conn_state_central_conn_count() == NRF_SDH_BLE_CENTRAL_LINK_COUNT)
            {
                bsp_board_led_off(CENTRAL_SCANNING_LED);
            }
            else
            {
                // Resume scanning.
                bsp_board_led_on(CENTRAL_SCANNING_LED);
                scan_start();
            }
            // Update status of LEDs.
            //bsp_board_led_off(CENTRAL_SCANNING_LED);
            //bsp_board_led_on(CENTRAL_CONNECTED_LED);
        } break; // BLE_GAP_EVT_CONNECTED

        // Upon disconnection, reset the connection handle of the peer that disconnected, update
        // the status of LEDs, and start scanning again.
        case BLE_GAP_EVT_DISCONNECTED:
        {
            NRF_LOG_INFO("CENTRAL: Disconnected, handle: %d, reason: 0x%x",
                         p_gap_evt->conn_handle,
                       p_gap_evt->params.disconnected.reason);

            // Update the status of LEDs.
            bsp_board_led_off(CENTRAL_CONNECTED_LED);
            bsp_board_led_on(CENTRAL_SCANNING_LED);

            /*if (p_gap_evt->conn_handle == m_conn_handle_hrs_c)
            {
                ble_uuid_t target_uuid = {.uuid = BLE_UUID_HEART_RATE_SERVICE, .type = BLE_UUID_TYPE_BLE};
                m_conn_handle_hrs_c    = BLE_CONN_HANDLE_INVALID;

                err_code = nrf_ble_scan_filter_set(&m_scan, 
                                                   SCAN_UUID_FILTER, 
                                                   &target_uuid);
                APP_ERROR_CHECK(err_code);
            }*/
            
            if (ble_conn_state_central_conn_count() == 0)
            {
                //err_code = app_button_disable();
                //APP_ERROR_CHECK(err_code);

                // Turn off the LED that indicates the connection.
                bsp_board_led_off(CENTRAL_CONNECTED_LED);
            }
            
            scan_start();
            
            // Turn on the LED for indicating scanning.
            bsp_board_led_on(CENTRAL_SCANNING_LED);
        } break; // BLE_GAP_EVT_DISCONNECTED

        case BLE_GAP_EVT_TIMEOUT:
        {
            // Timeout for scanning is not specified, so only connection attemps can time out.
            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
            {
                NRF_LOG_DEBUG("CENTRAL: Connection Request timed out.");
            }
        } break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
        {
            // Accept parameters requested by peer.
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                        &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("CENTRAL: GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("CENTRAL: GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for handling BLE Stack events that involves peripheral applications. Manages the
 * LEDs used to report the status of the peripheral applications.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
/* TL
static void on_ble_peripheral_evt(ble_evt_t const * p_ble_evt)
{
    ret_code_t err_code;
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("PERIPHERAL: Connected, handle %d.", p_ble_evt->evt.gap_evt.conn_handle);
            bsp_board_led_off(PERIPHERAL_ADVERTISING_LED);
            bsp_board_led_on(PERIPHERAL_CONNECTED_LED);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("PERIPHERAL: Disconnected, handle %d, reason 0x%x.",
                         p_ble_evt->evt.gap_evt.conn_handle,
                         p_ble_evt->evt.gap_evt.params.disconnected.reason);
            // LED indication will be changed when advertising starts.
        break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("PERIPHERAL: GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("PERIPHERAL: GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}
*/

/**@brief Function for handling advertising events.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
/* TL
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            bsp_board_led_on(PERIPHERAL_ADVERTISING_LED);
            bsp_board_led_off(PERIPHERAL_CONNECTED_LED);
            break;

        case BLE_ADV_EVT_IDLE:
        {
            ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
            APP_ERROR_CHECK(err_code);
        } break;

        default:
            // No implementation needed.
            break;
    }
}
*/

/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint16_t conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
    uint16_t role        = ble_conn_state_role(conn_handle);

    /* TL
    if (    (p_ble_evt->header.evt_id == BLE_GAP_EVT_CONNECTED)
        &&  (is_already_connected(&p_ble_evt->evt.gap_evt.params.connected.peer_addr)))
    {
        NRF_LOG_INFO("%s: Already connected to this device as %s (handle: %d), disconnecting.",
                     (role == BLE_GAP_ROLE_PERIPH) ? "PERIPHERAL" : "CENTRAL",
                     (role == BLE_GAP_ROLE_PERIPH) ? "CENTRAL"    : "PERIPHERAL",
                     conn_handle);

        (void)sd_ble_gap_disconnect(conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);

        // Do not process the event further.
        return;
    }
    */

    on_ble_evt(conn_handle, p_ble_evt);

    /* TL
    if (role == BLE_GAP_ROLE_PERIPH)
    {
        // Manages peripheral LEDs.
        on_ble_peripheral_evt(p_ble_evt);
    }
    else if ((role == BLE_GAP_ROLE_CENTRAL) || (p_ble_evt->header.evt_id == BLE_GAP_EVT_ADV_REPORT))
    {
        on_ble_central_evt(p_ble_evt);
    }
		*/
		if ((role == BLE_GAP_ROLE_CENTRAL) || (p_ble_evt->header.evt_id == BLE_GAP_EVT_ADV_REPORT))
    {
        on_ble_central_evt(p_ble_evt);
    }
}

/**@brief Battery level Collector Handler.
 */
// TL //
static void bas_c_evt_handler(ble_bas_c_t * p_bas_c, ble_bas_c_evt_t * p_bas_c_evt)
{
    ret_code_t err_code;

    switch (p_bas_c_evt->evt_type)
    {
        case BLE_BAS_C_EVT_DISCOVERY_COMPLETE:
        {
            err_code = ble_bas_c_handles_assign(p_bas_c,
                                                p_bas_c_evt->conn_handle,
                                                &p_bas_c_evt->params.bas_db);
            APP_ERROR_CHECK(err_code);

            // Battery service discovered. Enable notification of Battery Level.
            NRF_LOG_DEBUG("Battery Service discovered. Reading battery level.");

            err_code = ble_bas_c_bl_read(p_bas_c);
            APP_ERROR_CHECK(err_code);

            NRF_LOG_DEBUG("Enabling Battery Level Notification.");
            err_code = ble_bas_c_bl_notif_enable(p_bas_c);
            APP_ERROR_CHECK(err_code);

        } break;

        case BLE_BAS_C_EVT_BATT_NOTIFICATION:
            NRF_LOG_INFO("m_connected_peers[m_conn_handle].address %X:%X:%X:%X:%X:%X AddrType:0x0%d",
                        m_connected_peers[p_bas_c_evt->conn_handle].address.addr[5], 
                        m_connected_peers[p_bas_c_evt->conn_handle].address.addr[4], 
                        m_connected_peers[p_bas_c_evt->conn_handle].address.addr[3], 
                        m_connected_peers[p_bas_c_evt->conn_handle].address.addr[2], 
                        m_connected_peers[p_bas_c_evt->conn_handle].address.addr[1], 
                        m_connected_peers[p_bas_c_evt->conn_handle].address.addr[0]);
            
        
            NRF_LOG_INFO("Battery Level received %d %%. conn_handle: %X", p_bas_c_evt->params.battery_level, p_bas_c_evt->conn_handle);
        
            if (peri_bonded(&m_connected_peers[p_bas_c_evt->conn_handle].address) == true)
            {
                NRF_LOG_INFO("Disconnect");
    
                err_code = sd_ble_gap_disconnect(p_bas_c_evt->conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
                        
                APP_ERROR_CHECK(err_code);
            }
        
            //NRF_LOG_INFO("Disconnect");
    
            //err_code = sd_ble_gap_disconnect(p_bas_c_evt->conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
                        
            //APP_ERROR_CHECK(err_code);
        
            break;

        case BLE_BAS_C_EVT_BATT_READ_RESP:
            NRF_LOG_INFO("Battery Level Read as %d %%.", p_bas_c_evt->params.battery_level);
        
            if (peri_bonded(&m_connected_peers[p_bas_c_evt->conn_handle].address) == true)
            {
                NRF_LOG_INFO("Disconnect");
    
                err_code = sd_ble_gap_disconnect(p_bas_c_evt->conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
                        
                APP_ERROR_CHECK(err_code);
            }
            //NRF_LOG_INFO("Disconnect");
    
            //err_code = sd_ble_gap_disconnect(p_bas_c_evt->conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        
            //APP_ERROR_CHECK(err_code);
        
            break;

        default:
            break;
    }
}
// TL //


/**@brief Function for initializing the Heart Rate Service client. */
/* TL
static void hrs_c_init(void)
{
    ret_code_t       err_code;
    ble_hrs_c_init_t hrs_c_init_obj;

    hrs_c_init_obj.evt_handler = hrs_c_evt_handler;

    err_code = ble_hrs_c_init(&m_hrs_c, &hrs_c_init_obj);
    APP_ERROR_CHECK(err_code);
}
*/

/**
 * @brief Battery level collector initialization.
 */
// TL //
static void bas_c_init(void)
{
    ble_bas_c_init_t bas_c_init_obj;

    bas_c_init_obj.evt_handler = bas_c_evt_handler;

    ret_code_t err_code = ble_bas_c_init(&m_bas_c, &bas_c_init_obj);
    APP_ERROR_CHECK(err_code);
}
// TL //

/**@brief LED Button collector initialization. */
static void lbs_c_init(void)
{
    ret_code_t       err_code;
    ble_lbs_c_init_t lbs_c_init_obj;

    lbs_c_init_obj.evt_handler = lbs_c_evt_handler;

    for (uint32_t i = 0; i < NRF_SDH_BLE_CENTRAL_LINK_COUNT; i++)
    {
        err_code = ble_lbs_c_init(&m_lbs_c[i], &lbs_c_init_obj);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupts.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

     // Configure the BLE stack by using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for initializing the Peer Manager. */
static void peer_manager_init(void)
{
    ble_gap_sec_params_t sec_params;
    ret_code_t err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_params, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_params.bond           = SEC_PARAMS_BOND;
    sec_params.mitm           = SEC_PARAMS_MITM;
    sec_params.lesc           = SEC_PARAMS_LESC;
    sec_params.keypress       = SEC_PARAMS_KEYPRESS;
    sec_params.io_caps        = SEC_PARAMS_IO_CAPABILITIES;
    sec_params.oob            = SEC_PARAMS_OOB;
    sec_params.min_key_size   = SEC_PARAMS_MIN_KEY_SIZE;
    sec_params.max_key_size   = SEC_PARAMS_MAX_KEY_SIZE;
    sec_params.kdist_own.enc  = 1;
    sec_params.kdist_own.id   = 1;
    sec_params.kdist_peer.enc = 1;
    sec_params.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_params);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}





/** @brief Function for accepting or rejecting a numeric comparison. */
static void num_comp_reply(uint16_t conn_handle, bool accept)
{
    uint8_t    key_type;
    ret_code_t err_code;

    if (accept)
    {
        NRF_LOG_INFO("Numeric Match. Conn handle: %d", conn_handle);
        key_type = BLE_GAP_AUTH_KEY_TYPE_PASSKEY;
    }
    else
    {
        NRF_LOG_INFO("Numeric REJECT. Conn handle: %d", conn_handle);
        key_type = BLE_GAP_AUTH_KEY_TYPE_NONE;
    }

    err_code = sd_ble_gap_auth_key_reply(conn_handle,
                                         key_type,
                                         NULL);
    APP_ERROR_CHECK(err_code);
}


/** @brief Function for handling button presses for numeric comparison match requests. */
static void on_num_comp_button_press(bool accept)
{
    // Check whether any links have pending match requests, and if so, send a reply.
    if (m_conn_handle_num_comp_central != BLE_CONN_HANDLE_INVALID)
    {
        num_comp_reply(m_conn_handle_num_comp_central, accept);
        m_conn_handle_num_comp_central = BLE_CONN_HANDLE_INVALID;
    }
		/* TL
    else if (m_conn_handle_num_comp_peripheral != BLE_CONN_HANDLE_INVALID)
    {
        num_comp_reply(m_conn_handle_num_comp_peripheral, accept);
        m_conn_handle_num_comp_peripheral = BLE_CONN_HANDLE_INVALID;
    }
		*/
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
static void bsp_event_handler(bsp_event_t event)
{
    // TL //ret_code_t err_code;
    switch (event)
    {
        case BSP_EVENT_KEY_0:
            /* TL
            err_code = ble_hrs_heart_rate_measurement_send(&m_hrs, 87);
            if ((err_code != NRF_SUCCESS) &&
                (err_code != NRF_ERROR_INVALID_STATE) &&
                (err_code != NRF_ERROR_RESOURCES) &&
                (err_code != NRF_ERROR_BUSY) &&
                (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
                )
            {
                APP_ERROR_HANDLER(err_code);
            }
						*/

            on_num_comp_button_press(true);
            break;

      case BSP_EVENT_KEY_1:
            on_num_comp_button_press(false);
            break;

      case BSP_EVENT_KEY_3:
            NRF_LOG_INFO("Bonded Device Count: %d", m_bonded_peripheral_count);
      
            for (uint8_t i = 0; i < m_bonded_peripheral_count; i++)
            {
                NRF_LOG_INFO("Device %d:", i);
                NRF_LOG_INFO("Address: %X:%X:%X:%X:%X:%X", 
                    m_bonded_peripheral[i].addr[5],
                    m_bonded_peripheral[i].addr[4],
                    m_bonded_peripheral[i].addr[3],
                    m_bonded_peripheral[i].addr[2],
                    m_bonded_peripheral[i].addr[1],
                    m_bonded_peripheral[i].addr[0]);
            }
            break;
      
        default:
            break;
    }
}


/**@brief Function for initializing buttons and LEDs.
 *
 * @param[out] p_erase_bonds  True if the clear bonding button is pressed to
 *                            wake the application up. False otherwise.
 */
static void buttons_leds_init(bool * p_erase_bonds)
{
    ret_code_t err_code;
    bsp_event_t startup_event;

    err_code = bsp_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);

    *p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}


/**@brief Function for initializing the GAP.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device, including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params = m_scan.conn_params;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the GATT module. */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function is passed to each service that may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Queued Write instances.
 */
static void qwr_init(void)
{
    ret_code_t         err_code;
    nrf_ble_qwr_init_t qwr_init_obj = {0};

    qwr_init_obj.error_handler = nrf_qwr_error_handler;

    for (uint32_t i = 0; i < NRF_BLE_LINK_COUNT; i++)
    {
        err_code = nrf_ble_qwr_init(&m_qwr[i], &qwr_init_obj);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for initializing the Connection Parameters module. */
static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID; // Start upon connection.
    cp_init.disconnect_on_fail             = true;
    cp_init.evt_handler                    = NULL;  // Ignore events.
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling database discovery events.
 *
 * @details This function is a callback function to handle events from the database discovery module.
 *          Depending on the UUIDs that are discovered, this function forwards the events
 *          to their respective services.
 *
 * @param[in] p_event  Pointer to the database discovery event.
 */

static void db_disc_handler(ble_db_discovery_evt_t * p_evt)
{
    // TL //ble_hrs_on_db_disc_evt(&m_hrs_c, p_evt);
    ble_bas_on_db_disc_evt(&m_bas_c, p_evt);
    
    ble_lbs_on_db_disc_evt(&m_lbs_c[p_evt->conn_handle], p_evt);
}



/**@brief Function for initializing the database discovery module. */
static void db_discovery_init(void)
{
    ret_code_t err_code = ble_db_discovery_init(db_disc_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Heart Rate service. */
/* TL
static void hrs_init(void)
{
    ret_code_t     err_code;
    ble_hrs_init_t hrs_init_params;
    uint8_t        body_sensor_location;

    // Initialize the Heart Rate Service.
    body_sensor_location = BLE_HRS_BODY_SENSOR_LOCATION_FINGER;

    memset(&hrs_init_params, 0, sizeof(hrs_init_params));

    hrs_init_params.evt_handler                 = NULL;
    hrs_init_params.is_sensor_contact_supported = true;
    hrs_init_params.p_body_sensor_location      = &body_sensor_location;

    // Require LESC with MITM (Numeric Comparison).
    hrs_init_params.hrm_cccd_wr_sec = SEC_MITM;
    hrs_init_params.bsl_rd_sec      = SEC_MITM;

    err_code = ble_hrs_init(&m_hrs, &hrs_init_params);
    APP_ERROR_CHECK(err_code);
}
*/

/**@brief Function for initializing the advertising functionality. */
/* TL
static void advertising_init(void)
{
    ret_code_t             err_code;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance      = true;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}
*/


/**@brief Function for initializing logging. */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing the timer. */
static void timer_init(void)
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details Handles any pending log or key operations, or both, then sleeps until the next event occurs.
 */
static void idle_state_handle(void)
{
    
    
    ret_code_t err_code;
    
    err_code = nrf_ble_lesc_request_handler();
    APP_ERROR_CHECK(err_code);
    
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}


static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt)
{
    if (p_evt->result != NRF_SUCCESS)
    {
        NRF_LOG_INFO("--> Event received: ERROR while executing an fstorage operation.");
        return;
    }

    switch (p_evt->id)
    {
        case NRF_FSTORAGE_EVT_WRITE_RESULT:
        {
            NRF_LOG_INFO("--> Event received: wrote %d bytes at address 0x%x.",
                         p_evt->len, p_evt->addr);
        } break;

        case NRF_FSTORAGE_EVT_ERASE_RESULT:
        {
            NRF_LOG_INFO("--> Event received: erased %d page from address 0x%x.",
                         p_evt->len, p_evt->addr);
        } break;

        default:
            break;
    }
}
/*
void fstorage_wait_flash(nrf_fstorage_t const * p_fstorage)
{
    // While fstorage is busy, sleep and wait for an event.
    while (nrf_fstorage_is_busy(p_fstorage))
    {
        NRF_LOG_INFO("%d", nrf_fstorage_is_busy(p_fstorage));
        //idle_state_handle();
    }
}
*/

/*
static void fstorage_read_peripheral_count(uint8_t* peri_count)
{
    ret_code_t err_code;
    
    err_code = nrf_fstorage_read(&fstorage, FLASH_PERIPHERAL_COUNT, peri_count, 8);
    
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_INFO("fstorage_read_peripheral_count failed.");
        return;
    }
    
    NRF_LOG_INFO("fstorage_read_peripheral_count.");
}

static void fstorage_write_peripheral_count(uint8_t peri_count)
{
    ret_code_t err_code;
    
    uint8_t data[1];
    
    data[0] = peri_count;

    err_code = nrf_fstorage_write(&fstorage, FLASH_PERIPHERAL_COUNT, data, fstorage_len_modifier(sizeof(peri_count)), NULL);
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_INFO("fstorage_write_peripheral_count failed.");
        return;
    } else
    {
        fstorage_wait_flash(&fstorage);
        NRF_LOG_INFO("Done.");
    }
    
    NRF_LOG_INFO("fstorage_write_peripheral_count.");
}
*/
static void fstorage_read_peripheral_addr(uint8_t* peri_addr, uint8_t peri_idx)
{
    ret_code_t err_code;
    
    uint32_t flash_peri_addr = FLASH_PERIPHERAL_ADDR_START + (peri_idx * fstorage_len_modifier(BLE_GAP_ADDR_LEN));
    
    err_code = nrf_fstorage_read(&fstorage, flash_peri_addr, peri_addr, BLE_GAP_ADDR_LEN);

    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_INFO("fstorage_read_peripheral_addr failed.");
        return;
    }
    
    NRF_LOG_INFO("fstorage_read_peripheral_addr.");
}

//static uint32_t addr_h;
//static uint32_t addr_l;

static uint32_t addr[2];

static void fstorage_write_peripheral_addr(uint8_t peri_idx, uint8_t* peri_addr)
{
    ret_code_t err_code;
    
    //uint32_t addr_h;
    //uint32_t addr_l;
    
    //addr_h = (peri_addr[5] << 8) + peri_addr[4];
    //addr_l = (((((peri_addr[3] << 8) + peri_addr[2]) << 8) + peri_addr[1]) << 8) + peri_addr[0];
    addr[1] = (peri_addr[5] << 8) + peri_addr[4];
    addr[0] = (((((peri_addr[3] << 8) + peri_addr[2]) << 8) + peri_addr[1]) << 8) + peri_addr[0];
    
    
    //memset(addr, 0x00, sizeof(addr));
    //memcpy(addr, peri_addr, BLE_GAP_ADDR_LEN);

    NRF_LOG_INFO("fstorage_write_peripheral_addr peri_addr: %X:%X:%X:%X:%X:%X",
        peri_addr[5],
        peri_addr[4],
        peri_addr[3],
        peri_addr[2],
        peri_addr[1],
        peri_addr[0]);

    uint32_t flash_peri_addr = FLASH_PERIPHERAL_ADDR_START + (peri_idx * fstorage_len_modifier(BLE_GAP_ADDR_LEN));

    NRF_LOG_INFO("flash_peri_addr: %d", flash_peri_addr);
    NRF_LOG_INFO("fstorage_len_modifier(BLE_GAP_ADDR_LEN): %d", fstorage_len_modifier(BLE_GAP_ADDR_LEN));
    NRF_LOG_INFO("peri_idx: %d", peri_idx);
    
    //NRF_LOG_INFO("addr_l: %d", addr_l);
    NRF_LOG_INFO("sizeof(addr): %d", sizeof(addr));
    //NRF_LOG_INFO("flash_peri_addr: %d (%d) (%d)", FLASH_PERIPHERAL_ADDR_START, peri_idx * fstorage_len_modifier(BLE_GAP_ADDR_LEN), fstorage_len_modifier(BLE_GAP_ADDR_LEN));
    //err_code = nrf_fstorage_write(&fstorage, flash_peri_addr, addr, fstorage_len_modifier(BLE_GAP_ADDR_LEN), NULL);
    //err_code = nrf_fstorage_write(&fstorage, flash_peri_addr, &addr_l, sizeof(addr_l), NULL);
    err_code = nrf_fstorage_write(&fstorage, flash_peri_addr, &addr, sizeof(addr), NULL);
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_INFO("fstorage_write_peripheral_addr failed.");
        return;
    } else {
        //fstorage_wait_flash(&fstorage);
        NRF_LOG_INFO("Done.");
    }
    
    NRF_LOG_INFO("fstorage_write_peripheral_addr.");
}

static void fstorage_add_peripheral(uint8_t* peri_count, uint8_t* peri_addr)
{
    static uint8_t m_data[8];
    
    //NRF_LOG_INFO("peri_count: %d ", *peri_count);
    
    //NRF_LOG_INFO("peri_count: %d ", *peri_count);
    
    //fstorage_write_peripheral_count(*peri_count);
    
    memset(m_data, 0x00, sizeof(m_data));
    memcpy(m_data, peri_addr, BLE_GAP_ADDR_LEN);
    
    //NRF_LOG_INFO("sizeof(&peri_addr): %d ", sizeof(*peri_addr));
    
    fstorage_write_peripheral_addr(*peri_count, m_data);
    
    NRF_LOG_INFO("sizeof(m_data): %d", sizeof(m_data));
    
    (*peri_count)++;
    
    NRF_LOG_INFO("peri_addr %X:%X:%X:%X:%X:%X Added to Flash",
        peri_addr[5],
        peri_addr[4],
        peri_addr[3],
        peri_addr[2],
        peri_addr[1],
        peri_addr[0]);
    
    NRF_LOG_INFO("m_data %X:%X",
        m_data[7],
        m_data[6]);
    NRF_LOG_INFO("m_data %X:%X:%X:%X:%X:%X Added to Flash",
        m_data[5],
        m_data[4],
        m_data[3],
        m_data[2],
        m_data[1],
        m_data[0]);
    
    NRF_LOG_INFO("fstorage_add_peripheral");
}

static void fstorage_delete_all_peripherals()
{
    ret_code_t err_code;
    
    err_code = nrf_fstorage_erase(&fstorage, FLASH_PERIPHERAL_ADDR_START, 1, NULL);
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_INFO("fstorage_delete_all_peripherals failed.");
        return;
    }
    
    //fstorage_write_peripheral_count(m_bonded_peripheral_count);
    
    //NRF_LOG_INFO("fstorage_delete_all_peripherals");
    
    //fstorage_wait_flash(&fstorage);
    //NRF_LOG_INFO("Done.");
}

/** @brief Delete all data stored for all peers. */
static void delete_bonds(void)
{
    ret_code_t err_code;

    NRF_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
    
    fstorage_delete_all_peripherals();
}

static void read_bonds(void)
{
    uint8_t addr[BLE_GAP_ADDR_LEN];
    //fstorage_read_peripheral_count(&m_bonded_peripheral_count);
    
    
    for (uint8_t i = 0; i < FLASH_MAX_PERIPHERAL; i++)
    {
        fstorage_read_peripheral_addr(addr, i);
        
        NRF_LOG_INFO("Bonded Address in Flash: %X:%X:%X:%X:%X:%X",
            addr[5],
            addr[4],
            addr[3],
            addr[2],
            addr[1],
            addr[0]);
        
        if (memcmp(addr, INVALID_BONDED_ADDRESS, sizeof(addr)) != 0)
        {
            memcpy(m_bonded_peripheral[i].addr, addr, sizeof(addr));
            m_bonded_peripheral_count++;
            
            NRF_LOG_INFO("Bonded Address in Flash: %X:%X:%X:%X:%X:%X",
            m_bonded_peripheral[i].addr[5],
            m_bonded_peripheral[i].addr[4],
            m_bonded_peripheral[i].addr[3],
            m_bonded_peripheral[i].addr[2],
            m_bonded_peripheral[i].addr[1],
            m_bonded_peripheral[i].addr[0]);
        } else
        {
            break;
        }
    }
    
    
}


int main(void)

{
    bool erase_bonds;
    ret_code_t err_code;
    
    nrf_fstorage_api_t * p_fs_api;
    
    p_fs_api = &nrf_fstorage_sd;
    

    // Initialize.
    log_init();
    
    err_code = nrf_fstorage_init(&fstorage, p_fs_api, NULL);
    APP_ERROR_CHECK(err_code);
    
    NRF_LOG_INFO("erase unit: \t%d bytes",      fstorage.p_flash_info->erase_unit);
    NRF_LOG_INFO("program unit: \t%d bytes",    fstorage.p_flash_info->program_unit);
    
    
    //fstorage_read_peripheral_count(&m_bonded_peripheral_count);
    //NRF_LOG_INFO("fstorage_read_periphearl_count value: %d", m_bonded_peripheral_count);
    
    
    timer_init();
    buttons_leds_init(&erase_bonds);
    power_management_init();
    ble_stack_init();
    scan_init();
    gap_params_init();
    gatt_init();
    conn_params_init();
    db_discovery_init();
    qwr_init();
    // TL //hrs_init();
    // TL //hrs_c_init();
    //lbs_c_init();
    bas_c_init();
    peer_manager_init();
    // TL //advertising_init();

    //static uint32_t m_data          = 0xBADC0FFE;
    //err_code = nrf_fstorage_write(&fstorage, 0x80000, &m_data, sizeof(m_data), NULL);
    //APP_ERROR_CHECK(err_code);
    
    // Start execution.
    NRF_LOG_INFO("LE Secure Connections example started.");
    
    

    if (erase_bonds == true)
    {
        delete_bonds();
        // Scanning and advertising is started by PM_EVT_PEERS_DELETE_SUCEEDED.
    }
    else
    {
        // TL //adv_scan_start();
        read_bonds();
        scan_start();
    }
    
    NRF_LOG_INFO("fstorage_read_periphearl_count value: %d", m_bonded_peripheral_count);

    // Enter main loop.
    for (;;)
    {
        idle_state_handle();
    }
}
